# Famagasság

* Definíció szerint a leghosszabb ág hossza, avagy a gyökértől való leghosszabb előforduló távolság
	* Egy csúcsú fánál 0
	* Egy éllel rendelkező fánál 1
	* ÜRES fánál -1, az általánosság kedvéért
	* az alábbi fánál 4

	```
         *
        /\
       *  *
      /\  /\
     * *  * *
         /
         *
         \
         *
	```

	* Absztrakt szinten megadva, rekurzívan (fáknál ez igen gyakori fajta algoritmus)
	
	```
    h(t : BinTree) : Z
      HA t = ÜRES
        return -1
      KÜLÖNBEN
        return max(h(t.left()), h(t.right())) + 1
	```

    * A gyökérrel megadott fa magassága annyi, mint a nagyobbik gyereke magassága +1 (önmaga miatt)
    * Az üres fa magassága -1, és ez ha rekurzív hívással jutunk üres részfához, akkor is konzisztens eredményt ad