# Rákövetkezés keresőfában

* Írjuk meg az adott csúcs esetén annak az inorder bejárás szerinti rákövetkezőjét visszaadó algoritmust bináris keresőfára
* Ez (bináris keresőfára) megegyezik a rendezés szerinti rákövetkezéssel
* Iteratív algoritmust írjunk
* Magát a Node-ot adjuk át, amiről feltehetjük, hogy egy helyes, a szülő csúcsokat is számon tartó bináris keresőfa egy csúcsa
* Ha nincs rákövetkező (mert a fa legnagyobb elemén hívtuk meg az algoritmust), térjünk vissza ÜRES-sel
* Megoldás:
	* Bal ág: ha nincs jobb gyerek, megnézem a szülőmnek melyik gyereke vagyok. Amíg a jobb voltam, addig biztos nagyobb voltam nála, azaz addig nem a szülő a rákövetkező. Amint a bal gyereke voltam, biztosan nagyobb nálam a szülő, de az ő jobb részfája már nagyobb nála is, tehát a legkisebb nálam nagyobb elem ez a szülő. Ha valahol ÜRES-et találtam, ki kell lépnem, mert ekkor úgy tűnik én voltam a legjobboldalibb elem
	* Jobb ág: ha van jobb gyerekem, elmegyek felé, hiszen nagyobb, mint én. De lehet, hogy ennek még van bal részfája, amiben szintén nálam nagyobb, de nála kisebb elemek vannak. Egész pontosan a bal részfa legbaloldalibb elemét keressük (ami szélső esetben a jobb oldali gyerekem)

	```
	next(p : BinTree) : BinTree
	  HA p.right() = ÜRES
	    parent := p.parent()
	    AMÍG parent != ÜRES és p = parent.right()
	      p := parent
	      parent := p.parent()
	    return parent
	  KÜLÖNBEN
	    p := p.right()
	    AMÍG p.left() != ÜRES
	      p := p.left()
	    return p
	```
