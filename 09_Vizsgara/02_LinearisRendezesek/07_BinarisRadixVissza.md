# Radix vissza bináris számokra, tömbre

## Elv

* Ez az eljárás az előzővel szemben nem az "oszd meg és uralkodj" elvét fogja követni -- azaz rekurzívan egyre kisebb egységekre és részfeladatokra bontani a teljes bemenetet és feladatot --, hanem hasonlóan a korábban tanult visszairányú rendezéshez, a teljes input ismételt előrendezésével közelítjük iteratívan a célállapotot
* d jegyű kulcsokat feltételezve a rendezendő elemeken ismét előrefelé, összesen d-szer megyünk végig viszont itt a kulcshierarchiában a d-ediktől az első felé, azaz visszafelé haladunk
* Az input tömböt jelöljük a-val. Tudjuk, hogy az utolsó jegy szerint két diszjunkt halmazba (edénybe) válogathatók ki az elemek. Vezessünk be egy szintén n elemű b nevű tömböt: ennek a felső részét nevezzük ki a 0-s és az alsó részét az 1-es edénynek
* Azaz, az inputon sorrendben haladva, ha azt találjuk, hogy az adott elem utolsó jegye 0, a b elejére fűzzük (számon tartjuk egy segédváltozóval, épp melyik indexre tudunk beszúrni); ha pedig 1, a végére
* Mivel a és b mérete megegyezik, a b-ben a végén a két határ összeér -- ezt a közös indexet elmentjük -- és a b-ben így az a-beli elemek egy permutáltja lesz, amire igaz az, hogy 1) a 0-s végűek vannak elöl, az 1-es végűek hátul, 2) ettől eltekintve tartják az eredeti input sorrendjét
* Most b-ből indulunk ki és a-ba másoljuk át az elemeket hátulról a 2. jegy szerint
* Előbb végigmegyünk felülről lefelé az első edényen és az elemeket értelemszerűen a elejére ill. végére szortírozzuk (az egész a-t használjuk természetesen!)
* Majd a második edényen megyünk végig és töltjük fel a maradék helyeit
* Viszont itt nem felülről lefelé haladunk, hanem fordítva. Ezzel tudjuk biztosítani, hogy a rendezés stabil legyen -- amikor az elemeket az 1-es edénybe raktuk, azt alulról töltöttük fel, mivel előre nem lehetett tudni, hol lesz az edényhatár. Emiatt belekerült egy inverzió a rendszerbe, ezt most tudjuk úgymond semlegesíteni
* A teljes kulcshierarchián végigérve azt látjuk, hogy a 0-s edényben megfelelő sorrendben sorakoznak a globálisan legkisebb elemek, míg az 1-es edényben fordított sorrendben a legnagyobb elemek
* A megoldás az, hogy újra végigmegyünk a 0-s, majd az 1-es edényeken (előrefelé és visszafelé, ahogy szoktuk) de most mindent a képzeletbeli 0-s edénybe másolunk: azaz az első edényt érintetlenül hagyva, a második edényt pedig az elsőt követően invertálva másoljuk át
* Most már tényleg van egy rendezett tömbünk! Mivel az output a b, ezért amennyiben az a tömbnél ért minket a rendezettség, egy az egyben át kell másolnunk az elemeket a b-be -- ez páratlan d-nél fog így kijönni, a páros esetnél már az előző pontnál kész vagyunk
* Minden körben minden elemet mozgatunk, így egy kör műveletigénye n
* Összesen d jegy van, ezért d-szer előrendezünk, emellett van egy inverziós rendezés, és ha szükséges egy másolás
* Ez összesen tehát (d+1+χ(2∤d))*n kör, ami mivel d ∈ Ο(n), ϴ(n)-esnek mondható
    * A χ függvény egy logikai állítást vár, és 1-et ad vissza, ha az igaz, s 0-t, ha nem. Azaz páratlan d-kre van plusz egy másolás
* A rendezés hatékony, stabil, viszont 2n-es a tárigénye és nem helyben rendez

## Lejátszás

* Nézzünk egy n=7-es, d=3-as esetet
    * a = [101, 011, 111, 001, 010, 100, 001]
        * Ez az input, osszuk ketté a 3. jegy alapján
    * b = [010, 100, | 001, 001, 111, 011, 101]
        * Ez lett belőle, most másoljuk vissza a 2. jegy alapján a-ba
    * a = [100, 101, 001, 001 | 111, 011, 010]
        * Az 1. jegy alapján szortírozzunk b-be
    * b = [001, 001, 010, 011 | 111, 101, 100]
        * Az 1-es edényt az inverziók miatt fordított sorrendben átmásoljuk
    * a = [001, 001, 010, 011, 100, 101, 111]
        * Átmásoljuk b-be, mert az az output
    * b = [001, 001, 010, 011, 100, 101, 111]