# Radix Előre

## Feladat

* Kulcsokat akarunk rendezni (illetve nyilván az általuk reprezentált értékeket, amik a rendezés szempontjából lényegtelenek, ezért nem szerepeltetjük)
* A kulcsok hierarchikusak: minden kulcs több részből áll (mind ugyanannyiból!), és az egyes részek egy diszkrét halmaz elemeiből kerülnek ki (az i-edik kulcselem értékkészlete mindegyik kulcsnál ugyanaz, de az i-ediké és a j-ediké nem feltétlenül)
    * Példa: a kulcsok lehetnek hallgatók születésnapjai. A kulcs háromszintes. Az első szint a nagyobbtól a kisebb felé a születési év, ami a legidősebb és a legfiatalabb hallgató születési éve által meghatározott zárt intervallum. A második szint az [1..12] intervallum. A harmadik szint az [1..31] intervallum

## Megoldás

* Az input elemeit helyiértékek szerint hierarchikusan edényekbe rendezzük (a legnagyobb helyiértékkel kezdve)
* Alacsonyabb szinten mindig csak az adott felsőbb szintű edényt rendezzük tovább
* A lényeg most jön: ellentétben a "sima" edényrendezéssel itt "rekurzívan" az alacsonyabb szintű kulcsok szerint is ugyanezzel a fajta rendezéssel dolgozunk, hiszen a kulcs tulajdonsága miatt ezt megtehetjük
* A gond ezzel az, hogy sok edényt kell létrehozni, ami ha nem vagyunk ügyesek, sok tárpazarlással jár. Épp ezért ezt így ebben a formában nem igazán használják

## Példa

* A kulcsok most legyenek 3 betűs stringek: <bac, cac, acb, cca, aab, bab, cab, cbc, caa, aac>
* Azaz a kulcshierarchia 3 tagú, mindhárom szint értékkészlete az {a,b,c} karakterekből álló halmaz (ezek a "számjegyek" most)
* Mivel "előre", ezért először a kulcshierarchia legnagyobb értékével, az legbaloldalibb helyiértékkel foglalkozunk
* Ennek 3 értéke lehet, ezért 3 edénybe rendezzük őket (eredeti sorrendet tartva!)
* Figyelem, azért osztjuk 3 részre, mert ezen a szinten (és amúgy az összes többin is) 3 féle értékeket vehetnek fel a kulcselemek, és nem azért, mert 3 jegyű a kulcs!
    * a edény: <acb, aab, aac>
    * b edény: <bac, bab>
    * c edény: <cac, cca, cab, cbc, caa>
* Ha ez megvan, mindegyik edényből "lelógatunk" 3 újabb edényt, faszerkezetben:

```
  a      b      c
/ | \  / | \  / | \
a b c  a b c  a b c

```

* Ezekbe a második szintű edényekbe a rendre a megfelelő szülő csúcs elemei kerülnek a második helyiérték szerint csoportosítva
* Ezután ezt folytatjuk, egészen amíg el nem fogynak a jegyek, jelen esetben még egy emelet, összesen 3+9+27 darab, az alsó szinten már csak 0 vagy egy elemű edények lesznek. Az alsó szintű edényeket összefűzve megkapjuk a rendezett eredményt
* Láncolt ábrázolással jobb megvalósítani, mert nem tudjuk egy-egy edény mekkora