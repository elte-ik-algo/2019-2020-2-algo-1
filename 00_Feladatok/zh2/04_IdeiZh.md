# 2020-as második zh

* A zh-t online (canvas) írtuk 2020. 04. 21-én 16.00 és 16.35 között 30 perces időkerettel
    * (1) Mindenki kapott egy 10 pontos láncolt listás kódolós feladatot - választhatott, hogy struktogrammal vagy pszeudokóddal akarja beadni
    * (2) Mindenki kapott egy 5 pontos algoritmusjavítós feldatot, a hibák számára és típusára a feladat szövege utalt. Indokolni nem kellett, csak a lehető legegyszerűbben (legkevesebb eltéréssel) javítani a hibákat vagy furcsaságokat. Lehetett Teamsen is küldeni, ha valaki képként akarta
    * (3) Végül mindenki kapott egy szintén 5 pontos feladatot, ahol két kérdésre kellett válaszolni indoklással
* A zh a rendszer szerint 30 pontos volt, de mivel a struktogram és a pszeudokód vagy-vagy viszonyban volt egymással, így igazából 20 pontos volt
* A feladatokat random osztotta a rendszer, az alábbiakban feladatcsoportonként megadom az összes lehetséges feladatot és azok megoldásait
* Az egyes hallgatóknál a canvason előfordul, hogy viszonylag szűkszavúan értékeltem - a több helyen előforduló hibákról itt tudtok majd olvasni

## Első feladatcsoport - Kódolós

### Első lehetséges variáció

#### Feladat

* Írj algoritmust, ami egy paraméterként megadott tömböt és egy vele azonos elemtípusú egyszerű láncolt listát hasonlít össze. Akkor térjen vissza igazzal, ha a tömb 0. indexén ugyanaz az elem szerepel, mint a láncolt lista utolsó elemének kulcsa, a tömb 1. indexén az az elem, ami a láncolt lista utolsó előtti elemének a kulcsa, stb., tehát a láncolt lista fordított sorrendben tartalmazza a tömb elemeit. Minden más esetben legyen hamis a visszatérési érték. Törekedjünk a minimális műveletigényre.

#### Megoldás

* A feladat kissé gonoszan volt megfogalmazva, a lényeg az, hogy a tömbön jó műveletigénnyel végig tudunk menni bármilyen sorrendben, de a láncolt listát hasonlóan kedvező műveletigénnyel csak előrefelé tudjuk bejárni
* Azaz, ha a "láncolt lista fordított sorrendben tartalmazza a tömb elemeit", akkor a tömb is fordított sorrendben tartalmazza a láncolt lista elemein: a láncolt listán menjünk végig előrefelé és a tömbön visszafelé
* Több mindenre kell figyelnünk
    * Mehetünk úgy, hogy egy for ciklussal bejárjuk a tömböt, a ciklusmagban mindig növeljük p-t, és csekkoljuk persze, nem NULL-e. A végén pedig épp azt hogy NULL-e (különben nem lenne azonos a méretük)
    * Csinálhatnánk azt is, hogy a listát járjuk be és a ciklusmagban kezeljük a tömbben való tovább (vagyis vissza-)lépést
    * Mi végül a kettőt "egyszerre" csináljuk, így:

```
reverseEquals(a : T[n], l : E1<T>*) : L
  p := l
  i := n-1
  AMÍG p != NULL és i >= 0
    HA p->key != a[i]
      return false
    p := p->next
    i := i-1
  return p = NULL és i = -1
```

* Minden körben ellenőrzöm, hogy még mindkét bejáró értelmes helyen van-e, ha igen, megnézem azonosak-e az aktuális értékek; ha már nem, azt kell nézni egyszerre fogytak-e el
* Üres listára el se indulunk, éppúgy üres tömbre se és akkor térünk vissza igazzal, ha ez két kondíció egyszerre állt fenn

### Második lehetséges variáció

#### Feladat

* Írjunk algoritmust, ami egy megadott fejelemes, ciklikus, egész számokat tartalmazó láncolt listában egy k természetes szám alapján minden k-adik kulcsot kicserél k-ra. k=2 esetén tehát a 2., 4., 6., stb. adatokat cserélje ki. k=0 esetén semmit. Mi a műveletigény listahossz és k függvényében? Röviden indokolj

#### Megoldás

* A "k. adat" nyilván úgy értendő, hogy az első, második, harmadik, stb. értelmes adat, tehát az első a fejelem utáni, és így tovább
* Felmerülhet az a kérdés, hogy ha egyszer bejártuk a listát, be kell-e járni újra. Pl, ha a lista 5 elemű (fejelem nélkül számolva) és k = 2, akkor az világos, hogy a 2. és a 4. elemet át kell írni, de a 4. "utáni" második elem az 1., és utána a 3., tehát voltaképpen ezzel a logikával a lista minden elemét bejárjuk, ami ellentmondás. Ha pedig k osztója n-nek, akkor ilyesmi nem történik, de végtelen ciklusba kerülünk, ezt meg egész trükkös dolog ellenőrizni, hiszen akár lehetett egy mező értéke kezdetben is k, nem elég csak ezt nézni. Summa summarum, nem kellett ciklikusan bejárni a listát

```
changeByKToK(f : E1C<Z>*, k : N)
    HA k != 0
        p := f->next
        AMÍG p != f
            i := 1
            AMÍG i < k és p != f
                p := p->next
                i := i+1
            HA p != f
                p->key := k
                p := p->next
```

* A generikus paramétert nem kell kitenni, csak a tisztázás kedvéért tettem meg
* Tehát kezdünk a fejelem utáni első elemtől és megyünk a fejelem újbóli felbukkanásáig
* Elég 1-től k-1-ig menni, mivel a fejelem utáni első elem már az első, ha k = 1, kész is vagyunk, ha 2, akkor 1-et kell még mennünk, stb.
* Azért nem "for" ciklust írtam, mert nemcsak k-tól függ a bejárandó intervallum, hanem a lista hosszától is, a struktogramnyelv pedig nem engedi a break parancsot, ettől függetlenül persze értékeltem azt is, ha valaki így oldotta meg
* Vigyázni kell, hogy ellenőrizzük, elértük-e az f-et. Ezt látszólag sokszor kell, de mindegyiknek oka van: a külső körökben is, és a belsőben is nézem, el tudok-e számolni k-1-ig
* Ha az első k-adik elemet átírtuk, folytatjuk a külső ciklust. De eleve már az első elemtől indultunk, akokor ezen logika alapján most a k+1. elemtől kell. Ezért növeltem meg még p-t (kicsit olyan ez, mint az előreolvasás). Ha i-t 0-ról kezdtem volna, ez elhagyható lenne
* Ha k = 0, akkor nem kell csinálni semmit. Ehhez bevezettem egy elágazást, hiszen enélkül a k=0 úgy működne, mint a k=1, minden elemet átírna k-ra
* Megoldható lett volna úgy is, ha csak egy ciklus van, ahol számláljuk a lista hosszát, és amikor i éppen k-val osztható, akkor átírjuk a kulcsot k-ra (sőt, ez jóval egyszerűbb kód lenne)
* Műveletigény:
    * Ha k = 0, akkor konstans (tehát n-re a minimális műveletigény 1)
    * Ha k nem 0, akkor egyesével számolunk el k-ig (vagyis k-1-ig), de ezt összesen annyiszor, ahogyszor k megvan n-ben, tehát gyakorlatilag n lineáris függvénye ekkor a műveletigény - ha k véletlen nagyobb, mint n (aminek bár értelme nincs, de lehet, tehát gondolnunk kell rá), akkor a belső ciklus a fejelem megtalálása miatt lép majd ki, tehát akkor is n lépést teszünk meg
    * Arra vigyázzunk, hogy az nem jó érvelés, ha n = 0, akkor a műveletigény konstans, mivel az ugyanúgy n lineáris függvénye! A k más

### Harmadik lehetséges variáció

#### Feladat

* Írjunk algoritmust, ami egy megadott fejelemes, ciklikus, kétirányú lista első és utolsó elemét megcseréli egymással. Ne csak a kulcsokat írjuk felül, effektíve láncoljuk át a két elemet. Mi a műveletigény listahossz függvényében? Röviden indokolj.

#### Megoldás

* Üres vagy egy elemű listánál nincs dolgunk. Mivel ez fejelemes és ciklikus, akkor és csak akkor üres, ha a fejelem nextje saját maga. És ha 1 elemű, akkor a fejelem prevje egyenlő a fejelem nextjével... Figyeljétek, hogy fogalmaztam, ez visszafelé már nem igaz, ha a fejelem nextje egyenlő a fejelem prevjével akkor az előző tételből kiindulva üres is lehet a lsita vagy egy elemű. De nekünk épp ez kell, tehát az őrfeltétel meg is van
* Most ezek után mit tehetünk? Vagy kiláncoljuk mindkettőt egyesével és utána be, vagy egyszerre tesszük meg ugyanezt
* A lényeg, hogy mivel az f->next az első és az f->prev az utolsó, és ezek garantáltan léteznek és a feltétel teljesülésével különböznek is, e kettőt kell érinteni, nem kell a listát bejárni semerre
* Itt álljon a második féle megoldás példaképp:

```
swap(f : E2C*)
    HA f->next != f->prev
        last := f->prev
        first := f->next
        last->prev->next := first
        first->next->prev := last
        first->prev := last->prev
        last->next := first->next
        last->prev := f
        first->next := f
        f->next := last
        f->prev := first
```

* Itt érdemes megjegyezni, hogy ez a konkrét szekvencia funkcionálisan akkor is tökéletesen működni fog, ha történetesen az előfeltételt elhagyjuk, csak feleslegesen fut le ez a néhány parancs
* Az f-et persze nem referencia szerint adjuk át, mert maga az f nem változik, csak az általa elért többi elem
* A műveletigény az alakjából is jól látszik: konstans (tehát teljesen mindegy, milyen hosszú a a lista, mindenképp a listahossztól független konkrét darab utasításból áll a megoldó program)

### Negyedik lehetséges variáció

#### Feladat

* Készíts egy algoritmust, ami egy megadott egyszerű listát átmásol egy (általad létrehozandó, kezdetben üres) fejelemes, ciklikus listába és ezzel tér vissza. Az eredeti lista maradjon érintetlen. A lista elemei biztosan csak természetes szám típusú kulcsokat és next pointert tartalmaznak. A másolásra nem áll rendelkezésre copy constructor, új listaelemet kell létrehozni és beállítani a keyt és a nextet. Az összesített műveletigény legyen lineáris

#### Megoldás

* Az E1 igazából E1<N>, és az E1::key() ilyenkor N típusú, hiszen tudjuk, hogy természetes számokat tartalmaz - bár ennek jelentősége nincs, csak azért írtram a feladatban, hogy tudjuk, könnyen, konstans időben másolható a kulcs
* Mindenképpen létrehozunk az elején egy fejelemet, ez ciklikus listaelem lévén magára fog nextelődni kezdetben. Ezt egyelőre így is hagyjuk, mert nem tudjuk, üres-e a lista (mindenesetre a fejelemet üres listára is létrehozzuk)
* Elkezdem bejárni az eredeti listát, mindig létrehozok egy elemet, beállítom, befűzöm és megyek tovább
* Ehhez egy last segédpointert hoztam létre, mert fenn akarom tartani az eredeti lista elemsorrendjét - konstans beszúrási költséggel
* Mindig az előzőleg beszúrt (elsőnek a fejelem) elem nextje frissül be, tehát a végén a last nextje még magára mutat - ezt kell orvosolnunk
* Tudatosítsuk magunkban, hogy ez üres l esetében is működik (sőt, szükséges is)

```
copy(l : E1*) : E1C*
    f := new E1C
    p := l
    last := f
    AMÍG p != NULL
        last->next := new E1C
        last := last->next
        last->key := p->key
        p := p->next
    last->next := f
    return f
```

## Második feladatcsoport - Algoritmusjavítós

### Első lehetséges variáció

#### Feladat

* Egy fejelemes, ciklikus listában akarjuk megállapítani a páros értékű kulcsok számát. A lista kulcsai természetes számok
* Javítandó:
    * 1p - 1 típushiba (fordítási hiba)
    * 1p - 1 hiba a számlálásban (invalid értéke lesz)
    * 1p - 1 felesleges kódrészlet (ebből hiba nem lenne, de nem kell)
    * 2p - Rossz a listabejárás (futás idejű hiba/hibák) - ez összesen 3 sort érint!

    ```
    countEven(f : FE1*) : N
    	p := f
    	AMÍG p != NULL
    		HA 2 | p->key
    			d := c
    			d := d+1
    			c := d
    		p->next
    	return c
    ```

#### Megoldás

* 1 típushiba (fordítási hiba)
    * Nincs "FE1" típus
* 1 hiba a számlálásban (invalid értéke lesz)
    * Nem inicializáltuk
* 1 felesleges kódrészlet (ebből hiba nem lenne, de nem kell)
    * Segédváltozó felesleges
* Rossz a listabejárás (futás idejű hiba/hibák) - ez összesen 3 sort érint!
    * Az f-et is beleszámoljuk a bejárásba, pedig az csak a fejelem
    * Mivel ciklikus, ha nem üres, soha nem lesz NULL
    * p->next csak le van írva, mint kifejezés, de nincs értékül adva p-nek

    ```
    countEven(f : E1C*) : N
    	c := 0
    	p := f->next
    	AMÍG p != f
    		HA 2 | p->key
    			c := c+1
    		p := p->next
    	return c
    ```

### Második lehetséges variáció

#### Feladat

* Egy egyszerű lista utolsó k értékű eleme utáni első elem értékét szeretnénk k-ra frissíteni, majd ezzel a frissített értékű listaelemmel visszatérni - kiláncolni nem kell. Ha nincs ilyen, akkor NULL-t
* Javítandó:
    * 1p - 1 konvencionális (de fontos) hiba, felesleges elem
    * 1p - 1 fordítási hiba (később okoz fordítási hibát, mint ahol a hiba van)
    * 2p - 1 funkcionális hiba (nem száll el tőle, de rossz eredményt ad)
    * 1p - 1 funkcionális hiba (amitől el is szállhat)

    ```
    updateAndGetAfterK(&l : E1*) : E1*
    	p := l
    	AMÍG p != NULL ÉS p->key != k
    		p := p->next
    	HA p = NULL
    		return NULL
    	KÜLÖNBEN
    		p->next->key := k
    		return p->next
    ```

#### Megoldás

* 1 konvencionális (de fontos) hiba, felesleges elem
    * A referencia szerinti paraméterátadás jele. Mivel a lista pointerét nem fogjuk átírni. Egyrészt mivel valamelyik elem "utáni" elemet fogjuk, ami ezt nem érinti; másrészt mivel nem az elemet, hanem a kulcsát módosítjuk
* 1 fordítási hiba (később okoz fordítási hibát, mint ahol a hiba van)
    * Nincs átadva a k - ott okoz hibát, ahol használnám
* 1 funkcionális hiba (nem száll el tőle, de rossz eredményt ad)
    * Az első és nem az utolsó k értéket keressük meg (ennek javításához bevezettem a pk-t, és mindenképp végigmegyek a listán, k megtalálása után is)
* 1 funkcionális hiba (amitől el is szállhat)
    * Nem ellenőrizzük, hogy van-e k utáni elem..., a returnhöz még nem is baj, mert ha nincs, NULL, és ezzel kell visszatérni, de az értékátíráshoz kell, hogy létezzen

    ```
    updateAndGetAfterK(l : E1*, k : T) : E1*
    	p := l
    	pk := NULL
    	AMÍG p != NULL
    		HA p->key = k
    			pk := p
    		p := p->next
    	HA pk = NULL
    		return NULL
    	KÜLÖNBEN
    		HA pk->next != NULL
    			pk->next->key := k
    		return pk->next
    ```

### Harmadik lehetséges variáció

#### Feladat

* Egy fejelemes, ciklikus lista utolsó k értékű elemét tartalmazó listaelemet szeretnénk kiláncolni és törölni a listából. Ha nincs ilyen elem, ne foglalkozzunk vele
* Javítandó:
    * 1p - 1 típushiba (fordítási hiba)
    * 1p - 1 típushiba (fordítási hiba)
    * 1p - 1 felesleges ellenőrzés (hiba nincs belőle, de nem szerencsés)
    * 1p - 1 hiányzó ellenőrzés (ami miatt elszállhat)
    * 1p - 1 potenciálisan invalid érték kiolvasása, amitől elszállhat

    ```
    removeK(f : E1, k : E1)
    	p := f
    	AMÍG p->next != f
    		HA p->next->key = k
    			pk := p
    		p := p->next
    	t := pk->next
    	HA pk->next != NULL
    		pk->next := pk->next->next
    	delete t
    ```

#### Megoldás

* 1 típushiba (fordítási hiba)
    * Nem pointerként adtuk át a fejelemet - sőt, nem is ciklikus elemként
* 1 típushiba (fordítási hiba)
    * Nem T-ként adtuk át k-t
* 1 felesleges ellenőrzés (hiba nincs belőle, de nem szerencsés)
    * Ha már megvan pk, nextje biztosan van, hiszen ez a törlendő elem (sőt még a next nextje is van, "legrosszabb" esetben a fejelem)
* 1 hiányzó ellenőrzés (ami miatt elszállhat)
    * Nem csekkolom, hogy pk értéke valid-e (a javított verzióban a következő pont miatt ez a nullcheck), itt a fejelemes-ciklikusság miatt a bejárást nem hozhat NULL-t, de itt ha egyszer se volt igaz a feltétel, pk egyszer se kapott értéket, emiatt lehet hát az értéke NULL (vagyis az eredetiben definiálatlan)
* 1 potenciálisan invalid érték kiolvasása, amitől elszállhat
    * pk nincs inicializálva (NULL-ra), ha nincs k kulcsú elem, akkor definiálatlan marad a pk pointer

    ```
    removeK(f : E1C*, k : T)
    	p := f
    	pk := NULL
    	AMÍG p->next != f
    		HA p->next->key = k
    			pk := p
    		p := p->next
    	HA pk != NULL
    		t := pk->next
    		pk->next := pk->next->next
    		delete t
    ```

## Harmadik feladatcsoport - Elméletibb kérdések

* Alapvetően 5 pont a teljesen helyes megoldásra járt
* 4 pont, ha az egyik helyes, a másikban kis hiba van
* 3 pont, ha az egyik helyes, a másik nem jó vagy mindkettőben kisebb hibák vannak
* 2 pont, ha mindkettőben van értelmes gondolat, vagy az egyik egész jó, de a másik semennyire
* 1 pont ha az egyikben van értelmes gondolat

### Első lehetséges variáció

#### Feladat

* Miért lesz garantáltan rendezett egy megfelelő módon, láncolt listák feletti összefuttatós algoritmussal elkészített uniózás eredménye?
* Tudunk-e bináris keresést (logaritmikus keresést) írni láncolt listára? Ha igen: hogyan? Ha nem: miért?

#### Megoldás

* Az összefuttatás előfeltétele, hogy a két input lista rendezett legyen. Ha "megfelelő módon" használjuk ezt az algoritmust, akkor mindig a két input még feldolgozatlan elemei közül a kisebbiket fogja feldolgozni adott körben. Ebből a kettőből következik, hogy a két input elemei rendezett sorrendben kerülnek feldolgozásra. Az uniózás során elkészülő lista így rendezett lesz
* A bináris keresés egy konkrét érték meglétét keresi egy rendezett gyűjteményben. Rendezett láncolt lista lehet, konkrét értéket is kereshetünk. Ha az input nem rendezett, akár rendezhetjük is plusz költségért. Viszont ennek a módszernek a lényege a vizsgálandó elemek számának folyamatos felezése, amit nagyon bajos lenne láncolt listára implementálni. Az első lépés a középső elem meghatározása - ehhez rögtön be kell járni a listát két pointerrel: eggyel ami megmondja a hosszát és még eggyel, amit csak minden második körben léptetünk, hogy meglegyen a középső elem. Ha a lista nem kétirányú, ez mégnehezebb lehet. Utána gyakorlatilag mindig újra kell számolni a sorszámokat és újra kell keresni az elejétől vagy a "felétől" indulva. Esetleg kiláncolásokat csinálhatunk, de ekkor az input nem immutable. Akárhogyanis, a módszer lényege a hatékonysága, amit annak köszönhet, hogy tömbön az indexek konstans idejű számolgatásával tudunk könnyedén nagyokat ugrani a tömb elemei között. Ezt láncolt listán nem tudjuk értelmesen reprodukálni
    * Érkezett olyan megoldás, hogy használjunk tömbös ábrázolást, hiszen tömbben konstans a k. elemre ugrás. Ez jó ötlet, de hiába tudjuk az adott elemből meghatározni, hogy hányadik elem a következő, a tömbbeli indexe ezt nem követi, azt már csak lineáris időben tudnánk kiszámolni
    * Ezt nem kellett ennyire részletesen leírni, inkább csak a későbbi okulás miatt írtam ennyit

### Második lehetséges variáció

#### Feladat

* Nagyságrendileg miért nem olcsóbb egy rendezett listában kulcsra keresni, mint egy rendezetlenben?
* Van-e nagyságrendi különbség egy fejelemes láncolt lista és egy tömb méretének lekérdezése között? Ha igen: miért, és hogyan lehet ezt kiküszöbölni? Ha nem: mi miatt van így?

#### Megoldás

* Rendezett gyűjteményben használhatjuk a logaritmikus keresést. Ez kezdetben a lista középső elemét választja ki, majd mindig felezi a még bejárandó elemeket. Viszont a listán még a méretének tudatában sem lehet ezt a folyamatot optimalizálni, muszáj egyesével bejárni valahány elemet. Hogy mennyit, az a lista méretének lineáris függvénye. A későbbi körökben segédpointerekkel azért tudunk ezen javítani, de összességében így is az első lépés linearitása miatt ez marad a műveletigény, az első lépés a domináns tag
    * A "sima", lineáris keresést rendezett és rendezetlen listában is folytathatjuk. Legjobb esetben a lista elején van a keresett elem, legrosszabb esetben be kell járni, átlagos esetben nagyjából a felénél van. Ez a rendezetlen esetre elég világos módon adja meg a lineáris átlagos műveletigényt
    * A rendezett lista esetében annyit tudunk optimalizálni, hogy azokban az esetekben is, ahol a keresett elemet nem tartalmazza a lista, átlagosan elég a lista feléig elmenni (elég az első nagyobb elemig menni). A rendezetlen listában ekkor végig kellett menni. Tehát ha az összes lehetséges n méretű inputot egymás mellé tennénk, és az alapján számolnánk átlagot, akkor a számláló és így az eredmény lényegesen kisebb lenne, de így is alapvetően a méret lineáris függvénye marad, tehát bár egyértelműen hatékonyabb, de nem nagyságrendi mértékben hatékonyabb a rendezett keresés
* A tömbre tekinthetünk úgy, mint egy típusra, aminek van "length" adattagja, amivel konstans időben le lehet kérni a fordítási időben kialakuló, fix méretét (kapacitását)
    * Ezzel szemben a láncolt listának nincs ilyenje - eleve megvalósítás szinten nincs is lista típus, csak listaelem típus
    * Ahhoz, hogy egy listáról kiderüljön a mérete, pont annyit kell lépni, amennyi a mérete, ti. az első NULL-ig kell menni és számolni hány lépés volt
    * Ez fejelemesnél is így van
    * Optimalizálható, ha a fejelembe elmentjük a méretet és ezt minden méretet módosító lépésnél frissítjük. Mivel a fejelem elérése konstans, ezért ez a frissítés önmagában konstans. És hasonló okokból a lekérdezés is az. Ez persze csak akkor mehet, ha a lsita elemtípusa (a T) valami számszerű típus
        * Ha ez nem így van, akkor vezessünk be egy wrapper típust a lista fölé, aminek egyik adattagja a lista első eleme (vagy fejeleme), a másik pedig a mérete. Azért nem érdemes magába a listaelem-típusba felvenni a plusz adattagot, mert akkor feleslegesen az összes elemben ott lenne. Persze lehet ennek is gyakorlati haszna, de általában nincs