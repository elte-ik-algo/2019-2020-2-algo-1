# Algoritmusok és adatszerkezetek 1., második zh konzultáció

* Helye: MS Teams
* Ideje: 2020. 04. 14. kedd, 18.00 – 19.10 

## 1. feladat - Láncolt listás algoritmus

* Töröljük egy megadott kulcs utolsó előfordulását jelentő csúcsot egy kétirányú, nem fejelemes, nem ciklikus, nem rendezett listából! Térjünk vissza azzal, hogy sikeres volt-e a törlés
* Megoldás:
	* Végig kell mennünk egyesével a listán, és egy segédpointert minden alkalommal frissítenünk kell, amikor a megadott kulccsal rendelkező node-ot látogatunk. Ekkor ez a mutató a végén az utolsó találatra fog mutatni (vagy NULL-ra ha nem is volt)
	* Most mivel oda-vissza listánk van, nem kell se két pointer, se a dupla előrenézős módszer, hiszen bár szükségünk lesz a megtalált listaelem előzőjére a kiláncolás miatt, de azt le fogjuk tudni kérni belőle is
	* Mivel nem fejelemes a lista, várhatóan azt az esetet külön kell kezelnünk, amikor a lista első eleme a találat (hiszen neki nincs előzője)
	
    ```
    deleteLastOccurrence(&l : E2*, k : T) : L
    	t := NULL
    	p := l
    	AMÍG p != NULL
    		HA p->key == k
    			t := p
    		p := p->next
    	HA t == NULL
    		return false
    	KÜLÖNBEN
    		HA t == l
    			l := t->next
    			HA l != NULL
    				l->prev := NULL
    			delete t
    			return true
    		KÜLÖNBEN
    			t->prev->next := t->next
    			HA t->next != NULL
    				t->next->prev := t->prev
    			delete t
    			return true
    ```
	
	* l-t referencia szerint adjuk át, hiszen van olyan eset, amikor értéket adunk neki
	* Ha van találatunk, akkor kettéválunk aszerint, hogy ő az első elem-e vagy nem
	* Ha jobban megnézzük, a két ágban csak az a különbség, hogy l-nek adunk-e új értéket, vagy a találat előzőjének (nyilván t->prev létezik, ha t nem l)
	* A másik különbség nem is különbség: a t->next->prev-es értékadásnál a NULL nyugodtan helyettesíthető t->prevvel, ha t==l, mivel l-nek nincs előzője
	* Azaz a kód így egyszerűsíthető:
	
    ```
    deleteLastOccurrence(&l : E2*, k : T) : L
    	t := NULL
    	p := l
    	AMÍG p != NULL
    		HA p->key == k
    			t := p
    		p = p->next
    	HA t == NULL
    		return false
    	KÜLÖNBEN
    		HA t == l
    			l := t->next
    		KÜLÖNBEN
    			t->prev->next := t->next
    		HA t->next != NULL
    			t->next->prev := t->prev
    		delete t
    		return true
    ```
	
	* zh-n törekedjünk a minél rövidebb algoritmusra. Ami effektíve pontlevonást ér, ha az aszimptotikus műveletigény nagyobb, mint az elvárt. De ha csak konstansban tér el, netán kódismétlést tartalmaz, de a lépésszámra ez nem hat, az bár nem a legszebb megoldás, de a szempontunkból ugyanúgy hibátlan

## 2. feladat - Algoritmusjavítás

* Tekintsük az alábbi feladatot:
    * Itthon vagyok egész nap, így olvasni „kényszerülök”. Egy-egy fejelemes listában tárolom könyvcím alapján növekvőleg rendezve a nálam levő szórakoztató irodalmat és szakkönyveket (minden cím egyedi). Elképzelhető a két lista között átfedés – nyilván a karantén alatt mindenféle behatásokat kapó elme számára a „szórakoztató” fogalma erősen divergál eredeti jelentésétől. Értelemszerűen most szeretném elolvasni azokat a szakkönyveket, amik legalább 300 oldalasak és nem szórakoztatók. Helyezzük át ezeket egy új, hasonlóképp rendezett listába, a többi könyv pedig maradjon a helyén. A könyvek típusát jelöljük ```B```-vel és legyen egy ```p : N``` adattagja (oldalszám) a ```t : String``` (cím) mellett
* A megoldás elve:
    * Kell tehát két lista, legyen ```pro``` (professional) a szakkönyvek listája és legyen ```fun``` a szórakoztatóaké
    * Mindkettő fejelemes
    * Elő kell állítani egy új fejelemes változóba a ```pro\fun``` halmazkülönbséget, sőt, az eredeti ```pro``` listabeliek közül se kell mindent megtartani, csak ahol a ```p>=300```
    * ```pró```ból kiláncolhatok, a másik lista nem változik
* Ezt az algoritmust írtam fel:

```
getBoringBooks(pro : E1<B>*, fun : E1<B>*)
  bor := new E1<B>()*
  bor->next := NULL
  pPr := pro
  pp := pro->next
  fp := fun->next
  AMÍG pp != NULL és fp != NULL
    HA pp->key = fp->key
      pp := pp->next
      fp := fp->next
    HA pp->key > fp->key
      fp := fp->next
    HA pp->key < fp->key és pp->key->p >= 300
      pPr->next := pp->next
      pp->next := bor->next
      bor->next := pp
  return bor
```

* Állapítsuk meg a hibákat és javítsuk ki a kódot
* Megoldás:
    * Az algoritmus fejében rögtön van hiba
        * Az nem gond, ha kirakjuk a generikus típusparamétert (B), de nem is elvárás
        * Viszont ez nem "void", hanem egy függvény, tehát E1*-gal kéne visszatérnie
    * Ha bor egy pointer, akkor a bor := new E1* azt jelenti, hogy bor egy E1*-ra mutató pointer (azaz E1**), és most egy E1*-ot raktunk a heapre!
        * De nem, bor egy E1*, tehát helyesen bor := new E1 és nem bor := new E1*
    * A bor nextje a konstruktor miatt eleve NULL, nem kell kiírni külön - a key viszont definiáltatlan, ami nem baj, lévén ez egy fejelem
    * bPr szerepe itt még nem világos, később megmagyayrázom
    * pPr a p previousa, ami logikus, mert próból kiláncolhatok
    * Az összehasonlításoknál kell a ".t" adattag, mint cím, mert nem a könyveket, hanem a címüket hasonlítjuk
    * Az összehasonlítások nem egymás utáni egyenrangú ifek, hanem egy 3 ágú elágazás tagjai - ezért a "VHA"
    * Az első ágban (ágak sorrendje mindegy) elfelejtettük léptetni a pPr-t, ezzel megbontva az invariánst, miszerint az p megelőzője
    * A második ág, csoda, de jó!
    * A harmadik ágon több gond is van
        * Eleve, szereptévesztésben van az elágazás... itt azt az esetet kell lefedni, amikor a pro-beli "jön", azaz pro-beli, de nem fun-beli az elem
        * Az, hogy amúgy még legalább 300 oldalas is a könyv, az már ezen belül vizsgálandó dolog
        * Ugyanis, ha pro\fun-beli 200 oldalas a könyv, akkor is kell valamit csinálni (továbbléptetni a pro pointereit), de ha ez nincs explicit kiírva, itt nem generálódik automatikusan üres else ág (ami itt amúgy se lenne elég)
        * Amúgy "pp->key->p" se korrekt, hiszen pp pointer, tehát pp esetében jogos a nyíl a szelektorfüggvényhez, de pp->key már nem pointer, ezért oda pontot kell írni
        * A másik gond, hogy itt teljesen korrektül a borba láncoljuk pp-t, de a bor lista elejére! Mivel bor is növekvő kéne legyen, ez így nem jó, épp a végére kéne
        * A végére láncolást viszont konstans műveletigénnyel kéne abszolválni, ezért kellett végül a borhoz is egy másik pointer

```
getBoringBooks(pro : E1<B>*, fun : E1<B>*) : E1<B>*
  bor := new E1<B>()
  bPr := bor
  pPr := pro
  pp := pro->next
  fp := fun->next
  AMÍG pp != NULL és fp != NULL
    VHA pp->key.t = fp->key.t
      pPr := pp
      pp := pp->next
      fp := fp->next
    VHA pp->key.t > fp->key.t
      fp := fp->next
    VHA pp->key.t < fp->key.t
      HA pp->key.p >= 300
        pPr->next := pp->next
        bPr->next := pp
        pp->next := NULL
        bPr := bPr->next
        pp := pPr->next
      KÜLÖNBEN
        pPr := pp
        pp := pp->next
  return bor
```

* Még tudunk picit egyszerűsíteni, mivel a 3. ág két belső ágának ugyanaz az utolsó sora (ha kicsit belegondolunk)

```
getBoringBooks(pro : E1<B>*, fun : E1<B>*) : E1<B>*
  bor := new E1<B>()
  bPr := bor
  pPr := pro
  pp := pro->next
  fp := fun->next
  AMÍG pp != NULL és fp != NULL
    VHA pp->key.t = fp->key.t
      pPr := pp
      pp := pp->next
      fp := fp->next
    VHA pp->key.t > fp->key.t
      fp := fp->next
    VHA pp->key.t < fp->key.t
      HA pp->key.p >= 300
        pPr->next := pp->next
        bPr->next := pp
        pp->next := NULL
        bPr := bPr->next
      KÜLÖNBEN
        pPr := pp
    pp := pPr->next
  return bor
```

## 3. feladat - Tanult fogalmak értelmezésével megoldható kérdezz-felelek feladatok

* Mit jelent az iterált típus?
    * Olyan adatszerkezet, ami azonos típusú elemek gyűjteménye. Pl. egy tömb az mindig egy fix alaptípus feletti tömb, nem lehet egy tömbnek egyszerre string és int eleme is. A láncolt listáknál ez hasonlóan működik, ezt szoktam generikus típusparaméterekkel, kacsacsőr között jelölni
* Láncolt listára vagy tömbre jobb a beszúró rendezés műveletigénye? Miért?
    * Nagyságrendileg mindkettő négyzetes. Láncolt listára végigmegyünk az elemeken - ez egyszer n, majd minden elemet egy épülő rendezett listába rendezett módon beszúrunk - ez is n, mivel minden elemre hívjuk meg az utóbbit, ez a kettő szorzódik. Tömbnél egy részt elválasztunk, s az elválasztott rész utáni első elemet rendre összehasonlítjuk az elválasztott rész utolsó, utolsó előtt, stb. elemeivel. Ha az akutálisan beszúrandó elem nagyobb, a helyére shifteljük a rendezett rész aktuális elemét. Előbb-utóbb megállunk, az is a helyére kerül, a rendezett rész nő. Ezt is minden elemmel megcsináljuk, s egyszerre max. n elem shiftelődhet, ezért ez is négyzetes
* Mi biztosan nem NULL egy fejelemes listában?
    * Maga a listapointer, ami a "nulladik" elemre, a fejelemre mutat és hagyományosan f-fel jelöljük. Üres fejelemes listában is megvan legalább ez az elem, ezért ez sose NULL. Az utolsó elem nextje viszont NULL (kivéve ha ciklikus) és hasonlóan, ha kétirányú, de nem ciklikus, a fejelem prevje is NULL
* Igaz-e, hogy a pointerek, pl. a next pointer az E1 esetén mindig NULL-ra inicializálódnak?
    * Ez egy gonosz mondat. Általánosságban nem igaz - ha mást nem mondunk, definiálatlan az értékük, de az E1 nextje speciel tényleg NULLra inicializálódik, mert a konstruktorát így határoztuk meg
* Mennyi egy E1 copy constructorának műveletigénye?
    * Attól függ, mi feletti E1-ről beszélünk. Azt tudjuk, mindig van next és key. Ez eddig konstans lenne, de lehet, hogy a key összetett, vagy van más adattag is, ami potenciálisan nagy. Ezért nem nagyon szoktunk listaelemet másik listaelem másolatára inicializálni. Aztán a másik kérdés az, hogy egy listaelem egyben egy lista is, ha másoljuk, felmerül a deep copy igénye, azaz hogy valóban másolat legyen, ne történjen meg az, ha az eredeti listát módosítom, a másolat is módosul. Ezt ciklussal tudnánk megoldani, ami tovább drágítja a műveletet. Ezt tehát az absztrakt szintünkön nem tudjuk megmondani