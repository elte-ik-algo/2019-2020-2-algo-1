# 2020-as negyedik zh

* A zh-t online (canvas) írtuk 2020. 05. 22-én 17.00 és 17.35 között 30 perces időkerettel
    * Feladatok
        * (1) 2 db kérdés indoklással, bináris fákról, bináris keresőfákról 2-2 pontért
        * (2) 1 db igaz-hamis kérdés indoklással. 1 pont a válaszért és 2 pont az indoklásért
        * (3) Nevezetes bejárások alkalmazása 4 pontért
        * (4) Algoritmusírás 9 pontért
* A zh 20 pontos volt
* Részpontokat adtam
* A negyedik feladatnál alapvetően szöveges megoldásokat vártam, de Teamsen el lehetett a zh vége utáni 3 percben még fényképesen, ha valaki rajzolva akarta megoldani
* A feladatokat random osztotta a rendszer, az alábbiakban feladatcsoportonként megadom az összes lehetséges feladatot és azok megoldásait

## Első feladatcsoport - Kérdések indoklásokkal

* Adott egy olyan n csúcsszámű bináris fa, ahol minden csúcsának eltérő a címkéje. Hány különböző bejárása van ennek a fának? Miért?
    * Egy bináris fának 3+1 nevezetes bejárását tanultuk, de ettől még több bejárás lehetséges, mégha elképzelhető, hogy nem sok gyakorlati hasznuk van és/vagy kevésbé hatékonyan megvalósíthatók
    * A csúcsok címkéi mind eltérnek, ennek ellenére a nevezetes bejárások adhatnak azonos szekvenciát. Pl. a preorder és postorder sose lesz azonos (már ha n>1), mert előbbinél a gyökér az első, utóbbinál pedig épp az utolsó. De az inorderrel bármelyik lehet azonos, ha csak az egyik gyerek irányt használjuk. A szintfolytonossal is lehet bármelyik azonos
    * De ha n különböző címkéjű csúcsunk van, ennek továbbra is n! féle sorbarendezése lehet, tehát voltaképpen mindig n! féle bejárásunk van. Ez n=0-ra és 1-re 1-et ad, ami logikus, egy üres fát egyféleképpen tudunk bejárni (azzal, hogy nem csinálunk semmit), hasonlóan egy egy elemű fát is egy módon lehet bejárni (azzal, hogy kiírjuk a gyökerét)
* Ha egy csúcs inorder és postorder bejárás szerint is utolsó, akkor mit tudunk elmondani arról a csúcsról? Miért?
    * Az inorder "bal-én-jobb" sorrendet, míg a postorder "bal-jobb-én" sorrendet követve írja ki az elemeket. Mivel ez rekurzívan igaz, ezért a gyökérre is igaz
    * A postorder alapján tehát előbb kiírjuk a gyökér bal, majd jobb részfáját, majd ezek után a gyökeret és ezzel vége is a bejárásnak. Azaz ha egy csúcs a postorder szerint utolsó, az a gyökér
    * Az inorder szerint a gyökér "középen" van, minden ami utána van, az a jobb részfája. Azaz e két bejárásból ilyen esetben azt tudjuk, hogy ez a csúcs a gyökér, és a jobb részfája üres
* Lehet-e láncolt ábrázolású bináris fával megvalósítani a verem interfészt a szükséges függvényekkel és hatékonysággal? Ha igen, hogyan; ha nem, miért?
    * Igen, akár Node-dal, akár Node3-mal ábrázolva. Az üres verem az üres fa, amúgy pedig a veremtető a gyökérelem. Ha beszúrunk, akkor a gyökér pozíciójára szúrunk be, az eddigi gyökért (akár ha NULL is volt) pedig pedig az új gyökér bal gyerekeként állítjuk be. Ez konstans. Kivételkor kimentjük a gyökért egy segédváltozóba, átállítjuk a gyökért a bal gyerekére, majd felszabadítjuk a segédváltozó által mutatott területet. Ez is konstans. Gyakorlatilag a lista elejére szúrás, ill. elejéről elvétel algoritmusok ezek, ahol a next() megfelel a left()-nek. A jobb gyerekek végig NULL értékűek
    * BTW, tömbös ábrázolással is hasonlóan megoldható a dolog, bár ott inkább megfordítanám a sorrendet, tehát a levélcsúcsot szánnám veremtetőnek. Hiszen, ha a gyökér a veremtető, minden alkalommal shiftelni kéne a többi elemet, míg ha "alul" van, akkor csak ezt az egy csúcsot kell felülírogatni. Persze a veremtető megkeresése ekkor logaritmikus műveletigényű lenne, tehát ez esetben egy "utolsó kitöltött elem indexe" segédváltozót is karban kéne tartanunk, ekkor konstans lenne a műveletigény. Ilyet persze épeszű ember nem csinálna, hiszen n méretű fát ~2<sup>n</sup> méretű tömbbel tudnánk megadni teljesen feleslegesen, ráadásul mivel minden beszúrás növelné a magasságot, potenciálisan életbe lépne az amortizált műveletigény és át kéne másolni a tömböt egy új, kétszer akkora tömbbe
* Lehet-e tömbös ábrázolású bináris fával megvalósítani a sor interfészt a szükséges függvényekkel és hatékonysággal? Ha igen, hogyan; ha nem, miért?
    * Az előző pontban leírtam, hogy a vermet végülis lehetne, ha a veremtető a végén van, és mondjuk tudjuk előre a maximális kapacitást, hogy ne kelljen átméretezni a tömböt. Persze így is pazarló a kialakítás, de elméletben megoldható. Viszont a sornál nem tudjuk megoldani, hogy csak a levélszinten módosíthassunk elemeket, tehát a hatékonysági elvárások mellett lehetetlen a küldetés
    * BTW, láncolt ábrázolással, ha a levél elemre is van pointerünk, Node3-mal megoldható
* Lehet-e egyszerű láncolt listával reprezentálni egy bináris fát, ha a műveletekre nincsenek hatékonysági, csak funkcionális elvárásaink? Ha igen, hogyan; ha nem, miért?
    * Az egyszerű láncolt lista (SL) egy lineáris adatszerkezet, míg a fa legalábbis absztrakt szinten nem az. Ez gondot okozhat. Az utolsót kivéve minden listaelemnek egyértelműen egy rákövetkezője van, azaz láncolt listával egy fa egy bejárását tudjuk reprezentálni. Most az a kérdés, egy bejárásból tudunk-e reprodukálni fát: ha csak annyit tudunk, higy bináris fa, de más ismérvei nincsenek, akkor nem. A válasz tehát alapvetően az, hogy nem
    * Ha kétszeresen láncolt listát használunk (ami persze már nem SL), akkor tudjuk vajon valahogy kezelni minden listaelemre, hogy melyik elemek tartozzanak az ő bal (~prev) és jobb (~next) részfájba? Tudnánk, de akkor megkapnánk a hagyományos láncolt faábrázolást, azaz nem lenne lineáris az adatszerkeztet, gyakorlatilag nem lenne láncolt lista
    * Ugyanakkor tömbbel - ami szintén lineáris adatszerkezet - tudunk fát ábrázolni. Vajon tudunk-e tömböt láncoltan megadni (a fordítottjáról tanultunk csak)? Persze, de nyilván elveszítjük az indexelés hatékony lehetőségét, hiszen ha a k. elemre kell mennünk, k-1-et kell lépnünk a listában. Tehát nagyságrendlileg minden indexelés lineáris lesz a konstans helyett. Egy kicsit javíthatunk, ha kétirányú a lista, de akkor se nagyságrendi szinten. A tömbös faábrázolás gyakorlatilag a szintfolytonos bejárás szerinti sorrendnek felel meg, de van egy fontos kitétel: fizikailag a nemlétező csúcsoknak is fenntartjuk a helyet, hogy az indexelési szabályokat alkalmazni tudjuk és hogy ne kelljen shiftelgetni, tehát konstansok legyenek a műveletek. Tehát, ha egy láncolt listával a sorfolytonos bejárás szerinti sorrendben ábrázoljuk a csúcsokat, úgy, hogy a kimaradó csúcsokhoz is készítünk Node-okat valami extremális kulcsértékkel, akkor minden csúcsot egyértelműen azonosíthatunk. Ha most ezek után egy adott csúcsnak a szülőjére akarunk ugrani, akkor tudnunk kell annak a csúcsnak a sorszámát, amit vagy egy adattagként tarthatunk számon, vagy a csúcs megkeresésekor számítottuk ki, ebből a tanult képlet alapján kiszámoljuk a szülő "indexét", és ha kétirányú listánk van, akkor visszalépkedve, ha egyirányú, akkor a lista elejéről kiindulva előre lépkedve ennyit meg tudjuk találni a szülőt. Tehát végül is igen, megoldható
    * A tömbös szintfolytonos reprezentációt úgy is modellezhetjük láncolt listával, hogy a kimaradó csúcsok helyett nem szúrunk be helyőrzőket, de minden csúcsnál számon tartjuk annak szülőjét, gyerekeit, esetleg szintjét. A többi csúcs hivatkozása mehet pointerrel, indexeléssel, vagy akár csak a kulcs számon tartásával, ha feltételezhetjük, hogy ezek egyediek. Ez összességében "méglineárisabb" keresést adna, tehát mégkevésbé lenne hatékony, bár kisebb (ill. idétlenebb alakú) fáknál így is elég jó lenne, mivel bár lineáris a műveletigény, de a lineáris listában kevesebb töltelékelem lenne
* Bináris keresőfába készülünk beszúrni és tudjuk biztosan, hogy a beszúrandó elem nagyobb lesz, mint az összes kulcs ebben a fában. Ennek tudatában írhatunk optimalizált algoritmust ("insertToTheEnd" vagy valami ilyesmi). Tömbös vagy láncolt ábrázolással tudunk hatékonyabb algoritmust írni, vagy esetleg mindegy? Egyáltalán hatékonyabb-e ez, mint a rendes beszúrás? Miért?
    * Bináris keresőfa esetében mindig a levélszintre szúrunk be. Ez minimálisan konstans (ha a gyökérnek nincs jobb gyereke), átlagosan logos, maximálisan n-es (ha a gyökérnek nincs bal gyereke). Ha tudjuk, hogy biztosan mindegyik elemnél nagyobb az input, akkor is végig kell menni az eddig beszúrt elemeken, de annyival előrébb vagyunk, hogy tudjuk, hogy mindig jobbra
    * Tömbös ábrázolásnál voltaképpen az utolsó nemüres elem jobb gyerekére fogunk beszúrni, de ezt konstans időben nem fogjuk tudni kitalálni, mert nem feltétlen ez a fizikai legutolsó elem (a fa mérete nem feltétlenül egyenlő a tömb kapacitásával). Ugyanakkor, ha egy külön változóban számon tartjuk a legjobboldalibb elem indexét (~ fa méretét), vagy akár csak a fa magasságát, akkor tömbösen ez konstanssá tehető
    * Hasonlóan, láncolt esetben, ha egy változóban mindig számon tartjuk a legnagyobb elem Node-ját, akkor konstans lehet a műveletigény. Ekkor viszont Node3 célszerűbb, és akkor a maximum törlése is konstans tud lenni, hiszen itt a szülőre kell állíani ezt a pointert, illetve tőle kiláncolni a maximumot
    * Érkezett egy olyan megoldás is, ami kicsit csalás, de épp ezért nagyon ötletes, és mindenképpen elfogadható. Mivel tudjuk, hogy minden elemnél nagyobb lesz, egyszerűen csak szúrjuk be a gyökér helyére, és ami eddig a fa gyökere volt, az legyen ennek a bal gyereke. Ez tömbösnél lineáris, mert shiftelni kell, de láncoltnál konstans
* Egy bináris keresőfában mi a minimális, átlagos és maximális műveletigénye az adott kulcsra keresésnek a méret függvényében? Miért?
    * Bináris keresőfában a gyökérből indulunk ki, de az értéknek megfelelően mindig balra vagy jobbra megyünk csak, a részfákra szűkítve a még vizsgálandó elemeket. Ez átlagos esetben mindig a lehetőségek felezését jelenti, ezért ez kb. log<sub>2</sub>(n)-es lesz. Minimális esetben rögtön a gyökér a keresett elem, ekkor konstans, Maximális esetben pedig a leghosszabb ág végén derül ki, hogy pont azt az elemet kerestük, esetleg nem szerepelt a fában a kérdéses elem. Ez a leghosszabb ág lehet elfajult esetben annyi, mint a fa mérete, tehát legrosszabb esetben ez lineáris
    * Vigyázat, az nem jó legjobb esetnek, hogy a fa csak egy elemű, netán üres és ezért konstans a lépésszám, hiszen ekkor ez a konstans egyben az n értéke is, tehát ez lineáris ilyenkor
* A "rákövetkezés keresőfában" algoritmus (https://gitlab.com/elte-ik-algo/2019-2020-2-algo-1/-/blob/master/08_BinarisKeresofak/03_Rakovetkezes.md) esetében vagy a jobb gyerek, vagy a szülő felé indulunk. Akkor és csak akkor a jobb gyerek felé, ha létezik. Ha létezik szülő is és jobb gyerek is, az algoritmus szerint akkor is a jobb gyerek felé megyünk. Miért jó ez biztosan?
    * Ha a szülő felé megyünk, akkor egész pontosan az első olyan "ősét" keressük, aminek az adott kiinduló csúcs a bal részfájában van. Miért? Mert akkor annál kisebb. És valóban, ekkor ez az ős jó, mert az ő jobb gyerekei még nála is nagyobbak, a bal részfájának pedig a kiinduló elem a maximuma, hiszen a bal részfájának elemeinél nagyobb, a ciklusban érintett őseinek pedig a jobb részfája volt, tehát annál is nagyobb, annak bal gyerekeinél meg pláne... de várjunk csak..., nem beszéltünk az eredeti csúcs jobb részfájáról. Abban viszont nála nagyobb elemek vannak, egész pontosan a jobb részfájának minimuma (legbaloldalibb eleme) az, ami épp csak egy kicsit nagyobb az eredeti csúcsnál. Ha ez létezik és ilyen tulajdonságú ős is létezik, az a kérdés, melyik a nagyobb. Az eredeti csúcs jobb részfájának minimuma is az ős szempontjából annak bal részfájában van, tehát az ősnél az is kisebb. Tehát mit kaptunk? eredeti csúcs kulcsa < jobb részfájának minimális kulcsa < rákövetkező ősének kulcsa. Ebből következik, hogy a közvetlen rákövetkező akkor is a jobb részfa minimuma, ha amúgy van ilyen őse is

## Második feladatcsoport - Igaz-hamis indoklással

* Ha egy bináris fa mérete 2<sup>k+1</sup>-1 alakú, ahol k tetszőleges természetes szám, akkor ezt a fát hatékonyabb tömbösen tárolni, mint láncoltan. Ha igen, ha nem: miért?
    * Hamis
    * Alapvetően akkor jobb tömbösen ábrázolni, ha közel teljes fa (hogy ne legyenek feleslegesen lefoglalt elemek) és valószínű nem fog növekedni a magassága (mert akkor újra kellhet allokálni)
    * Ha k = h (mármint a fa magassága) akkor ez épp teljes fa. De ha k tetszőleges természetes szám, csak épp ilyen alakú, akkor az is lehet, hogy ez a fa egy lánc, tehát a foglalandó méret gyakorlatilag 2 az ennyiediken, ez nagyon pazarló, ekkor nem éri meg tömbösen ábrázolni
    * Tehát a válasz "attól függ", de általánosságban ebből semmiképpen sem kvöetkezik, hogy megérné
* Tudunk írni olyan algoritmust, aminek az inputja egy csupa bináris kifejezésekből álló helyes, prefix-lengyelforma, az outputja az ennek megfelelő RPN, és bináris fát használ a konverzióhoz. Ha igen, kb. írd le az algoritmus elvét; ha nem, indokold meg, ez miért nem lehetséges
    * Igaz
    * A prefix-lengyelforma a kifejezésfa preorder bejárása. Az RPN pedig a postorder bejárása. Tehát ha megvan a kifejezésfa, onnan már megvan az RPN is
    * Alapvetően prefix és infix bejárásból tudunk fát rekonstruálni, de kifejezésfa esetében ennél okosabbak tudunk lenni
    * Operátorok és operandusok lesznek a PN-ben. Ha operátort olvasok (ezzel fog kezdődni), akkor becsomagolom egy Node-ba és verembe rakom. Ha operandust, akkor szintén készítek hozzá egy Node-ot és megnézem a verem tetején operandusos Node van-e. Ha nem, akkor berakom. Ha viszont igen, akkor biztosan van egy operátor, felette az ő bal paramétere, tehát ezt kiveszem, mellécsapom az újonnan feldolgozandó elemet, beállítom ezeket a mégeggyel azalatti Node gyerekeinek és visszarakom azt a verembe, megjelölve úgy, hogy ő már egy részkifejezés. Majd megyek tovább, meglesz ebből a fa
* Megírtuk a nevezetes bejárások szerinti rákövetkezés függvényeket, külön optimalizálva tömbös és láncolt ábrázolásra is. Az összes ilyen függvény műveletigénye lineáris lett a famagasság függvényében. Ha igen, írd le mindről röviden, miért elkerülhetetlen ez; ha nem, keress ellenpéldát, és mutass rá, hogy az az
    * Hamis
    * 4 nevezetes bejárás van, vegyük ezeket sorra
    * Preorder, inorder, postorder: mind nagyon hasonló algoritmus, mint amit órán vettünk. Ezekben vagy részfa minimumát kell keresni, vagy szülőt rekurzívan, mindkettő egy ág bejárását jelenti, azaz a fa magasságával korrelál. Tömbös ábrázolással sem jártunk jobban, indexeket kell számolgatni, amik persze konstans időt vesznek igénybe, de famagasságszor konstans időt
    * Szintfolytonos: itt a láncoltnál megnézzük van-e testvér, ha nem, unokatestvér, stb..., ha semmi sincs, elindulunk újra a gyökértől és eggyel mélyebbre megyünk bal szélre, mint ahonnan indultunk, ez inkább már a csúcsok számával lesz lineáris. Tömbösen viszont csak a következő nemüres elemet kell megkeresni, ez legjobb esetben konstans, s bár legrosszabb esetben csúcsszám szerinti lineáris is lehet (ha csupa balgyerekes lánc a fa), de ezen akár optimalizálni is tudunk, ha elmentjük ezt minden csúcsra
* Ha adott egy kulcssorozat, amiről tudjuk, hogy egy bináris keresőfa preorder bejárása, akkor ebből egyértelműen helyreállítható a fa. Ha igen, hogyan; ha nem, miért?
    * Igaz   
    * A preorder rekurzívan én-bal-jobb sorrendet követ. Ha a sorozat üres, a fa üres; különben az első elem a gyökér. Utána jönnek a bal részfa elemei, de nem tudjuk mennyi, lehet akár egy se. Azt viszont tudjuk, hogy keresőfa lévén ezek fixen kisebbek a gyökrénél. Vegyük tehát addig az elemeket, amíg a gyökérnél kisebbek, majd ezt az algoritmust hívjuk meg rekurzívan a bejárás eredményének ezen substringjére a root.left()-tel, mint kitöltendő gyökérrel. A maradék rész a jobb részfa, erre ugyanez vonatkozik
* A "beszúrás keresőfába" algoritmus (https://gitlab.com/elte-ik-algo/2019-2020-2-algo-1/-/blob/master/08_BinarisKeresofak/02_Beszuras.md) egyszerűsíthető. Ha igen, mutasd meg, hol és miben. Ha nem, miért nem?
    * Igaz
    * Erre órán elég egyértelműen utaltam
    * Egy ciklussal kezdünk, melynek feltétele: AMÍG current != ÜRES és current.key() != p.key()
    * Azaz, amikor ennek a ciklusnak vége, vagy a current ÜRES lesz vagy a kulcsa nem egyenlő p kulcsával. Matematikailag lehetne mind a kettő, a gyakorlatban, ha current ÜRES, akkor nincs kulcsa, tehát akkor a jobb oldali lekérdezésnek nincs értelme
    * Tehát most így folytatjuk: HA current != ÜRES és current.key() = p.key()
    * Ha current nem ÜRES, akkor lehet-e egyenlő a kulcsa p kulcsával? Nem lehet, mert akkor még a ciklusban lennénk, tehát itt az éselés második tagját elhagyhatjuk
    * Tehát gyakorlatilag a current = ÜRES feltétel alá beírható a mostani második ág, majd a mostani első ág akár el is hagyható teljesen (feltételezve, hogy a nyelv generál implicit, üres else ágat)
    * Kicsit másképp is megfogalmazom. Mi most a KÜLÖNBEN ág feltétele? Nyilván a HA ág tagadása, ami: HA current = ÜRES vagy current.key() != p.key()
    * Ha current ÜRES, az tiszta sor. De a jobb tag nyilván impliciten azt mondja, hogy nem ÜRES a current, de a kulcsa se azonos a p kulcsával -> akkor hogy léptünk ki a ciklusból? Tehát ez az eset nem történhet meg
* A "beszúrás keresőfába" és "rákövetkezés keresőfában" algoritmusok (https://gitlab.com/elte-ik-algo/2019-2020-2-algo-1/-/tree/master/08_BinarisKeresofak) egyike se írható meg olyan reprezentációval, ahol nincs parent referencia. Az indoklásban mindkettőre külön-külön mondd el miért igen vagy miért nem igaz rá az állítás
    * Hamis
    * Ez a sima, Node-os láncolt ábrázolás
    * A beszúrásnál használjuk a parentet, de az, amit "parent" változóként használunk, az voltaképpen a parent() nélkül van kiszámolva, ez végülis a klasszikus két pointeres keresés prevje. Ha nem használnánk, akkor előrenézéses egy pointeres keresés kéne, de ez így a "két féle next" miatt nehézkes lenne. Tehát ez a parent nem az a parent. A végén még p parentjét beállítjuk erre a parent változóra. Ez nyilván sima Node-os reprezentációnál elhagyható, de ez nem feltétele, hanem inkább a terméke az algoritmusnak, a hiánya gondot nem okoz
    * Ami a rákövetkezést illeti, lehetne workaround erre is, de ez az algoritmus erősen épít a parent() meglétére, amikor visszafelé kell menni
    * A workaround az lenne, ha elkezdenénk lefuttatni egy rendes, rekurzív inorder bejárást, ahol maga a bejárás logikája tartja számon a szülő-gyerek viszonyt, és figyelnénk, mikor találjuk meg az ominózus elemet. De ez már egy másik algoritmus

## Harmadik feladatcsoport - Nevezetes bejárások rekonstrukciója

* Itt 4 féle feladat volt
* Adtam részpontokat, ha a bejárás emlékeztetett a rekonstruálandó fára
* A variációk:
    * Adott egy bináris fa, aminek a preorder bejárása ez volt: A,B,D,E,C,F,G,H,I az inorder pedig ez: D,E,B,A,F,C,H,I,G. Mi volt a postorder bejárása?
    * Adott egy bináris fa, aminek a postorder bejárása ez volt: E,D,B,F,I,H,G,C,A az inorder pedig ez: D,E,B,A,F,C,H,I,G. Mi volt a preorder bejárása?
    * Adott egy bináris fa, aminek a preorder bejárása ez volt: D,E,B,C,F,I,A,G,H az inorder pedig ez: D,C,B,I,F,E,A,G,H. Mi volt a postorder bejárása?
    * Adott egy bináris fa, aminek a postorder bejárása ez volt: C,I,F,B,H,G,A,E,D az inorder pedig ez: D,C,B,I,F,E,A,G,H. Mi volt a preorder bejárása?
* Látható, hogy az első két feladat erről a fáról szól:

    ```
        A
       / \
      B   C
     /   / \
    D   F   G
     \     /
      E   H
          \
           I
    ```

* A második kettő pedig erről:

    ```
      D
       \
        E
       / \
      B   A
     / \   \
    C   F   G
       /     \
      I       H
    ```

* És a megoldások is "keresztbe" kiolvashatók a feladatok szövegéből

## Negyedik feladatcsoport - Algoritmusírás

* Írjunk algoritmust, aminek a paramétere egy absztrakt módon megadott bináris fa gyökere, és azt ellenőrzi, ez a fa egy binér kifejezés kifejezésfája-e. Használjuk az előre definiált OPERATOR és OPERAND halmazokat. Ha nem helyes kifejezésfa, akkor térjünk vissza azzal, hogy "NEM", egyébként pedig a legtöbbet előforduló (vagy az egyik legtöbbet előforduló) műveleti jellel
    * Az egy jó kérdés, az üres kifejezés kifejezés-e. Talán inkább nem, de az biztos, hogy leggyakrabban előforduló operátora biztosan nincs, ezért most egész biztosan így tekintünk rá
    * Mivel akik ezt az algoritmust kapták, nem nagyon jutottak el a maximumkiválasztás részéhez, ezért előbb közlöm az algoritmust eme rész nélkül. Így, ha gépelve kell elkészíteni, amúgy is javasolt módszer apránként hozzáadni a "feature"-öket, mert kevésbé bonyolódunk bele

    ```
    isSyntaxTree(t : BinTree) : L
        HA t = ÜRES
            return false
        KÜLÖNBEN
            HA t.key() eleme OPERAND
                return t.left() = ÜRES és t.right() = ÜRES
            KÜLÖNBEN HA t.key() eleme OPERATOR
                return isSyntaxTree(t.left()) és isSyntaxTree(t.right())
            KÜLÖNBEN
                return false
    ```

* Tehát üres kifejezést nem tekintünk kifejezésnek
* Különben, ha a gyökér egy operandus, akkor maga az az érték a kifejezés, tehát akkor és csak akkor valid, ha nincs egy gyereke se
* Ha operátor, akkor mindkét gyerekének léteznie kell és vagy operandusnak kell lennie vagy saját jogán is értelmes operátoros kifejezésnek
* Ehhez rekurziót használunk, és mivel azt eleve elvártuk a felső szinten, hogy ne legyen üres a kifejezés, ezért ezt, hogy "egyik gyerek se legyen üres" nem is nézem külön, a rekurzív hívásra bízom ennek ellenőrzését
* Ez akkor is igazat ad, ha egy operátornak az egyik gyereke már levél és a másik gyereke egy másik operátor
* Még raktam egy különben ágat, ha esetleg nem operátor vagy operandus az adott csúcs
* A maximumkeresést alapvetően asszociatív tömbbel gondoltam volna, bár ilyet nem tanultunk - de épp emiatt adok hozzá egyszerű láncolt listás implementációt is:

    ```
    mostCommonOperatorOfSyntaxTree(t : BinTree) : String
        maxTable : MaxTable
        HA collectOperatorsOfSyntaxTree(t, maxTable)
            return maxTable.maxKey()
        KÜLÖNBEN
            return "NEM"
    ```
    
    ```
    collectOperatorsOfSyntaxTree(t : BinTree, maxTable : MaxTable) : L
        HA t = ÜRES
            return false
        KÜLÖNBEN
            HA t.key() eleme OPERAND
                return t.left() = ÜRES és t.right() = ÜRES
            KÜLÖNBEN HA t.key() eleme OPERATOR
                maxTable.add(t.key())
                return collectOperatorsOfSyntaxTree(t.left()) és collectOperatorsOfSyntaxTree(t.right())
            KÜLÖNBEN
                return false
    ```
    
    ```
    MaxTable
    - l : E1<(String, N)>*
    
    MaxTable::MaxTable()
        l := NULL
    
    MaxTable::add(s : String)
        p := l
        AMÍG p != NULL és p->key()₁ != s
            p := p->next
        HA p = NULL
            q := new E1
            q->key := (s, 1)
            q->next := l
            l := q
        KÜLÖNBEN
            p->key()₂ := p->key()₂ + 1
    
    MaxTable::maxKey()
        HA l = NULL
            HIBA
        KÜLÖNBEN
            max := l->key()
            p := l->next()
            AMÍG p != NULL
                HA p->key()₂ > max₂
                    max := p->key()
                p := p->next
            return max₁
    ```

    * Annyival változott az algoritmus, hogy amikor operátort olvasunk, azt berakjuk ebbe a gyűjteménybe, amit amúgy kívülről adunk át, hogy ne inicializálódjon minden rekurzív hívásra új
    * Ez pedig átnézi a listát, hogy van-e benne már az az operátor és az elejére szúr, ha nem. Ha pedig igen, növeli a multiplicitását
    * A maxKey() pedig átnézi a második paraméter (multiplicitás) alapján az elemeket, és visszatér a legnagyobbhoz tartozó karakterrel
    * A szándékaim szerint a típus megírása nem volt a feladat része, ezért újrapontoztam azokat a megoldásokat, akik ezt kapták, és viszonylag magas pontot kapott mindenki, aki csak ezt hagyta ki
* Írjunk algoritmust, aminek az egyik paramétere egy számokat tartalmazó 0-tól indexelt tömb, a másik paramétere pedig egy számokkal kulcsolt Node3, azaz egy láncolt ábrázolású bináris fa gyökere. Ellenőrizzük, hogy a két fa ugyanannak a fának a tömbös és láncolt megvalósítása-e. Azt is ellenőrizzük, hogy a szülő és gyerek pointerek konzisztensek-e. Legyünk minél hatékonyabbak
    * Az ideális megoldás szintfolytonos bejárásra épít, mert úgy van a tömbben is ábrázolva a fa
    * Ha a láncolt fa nullpointer, akkor megnézzük, a tömbben csupa üres elem van-e
    * Ha nem, akkor elkezdünk rajta egy hagyományos szintfolytonos bejárást, míg a tömbnél is 0-ra inicializáljuk az indexet, amivel bejárjuk
    * Sorra vesszük ki az elemeket, viszont itt most a NULL-okat is berakjuk, sőt minden NULL-hoz az ő két NULL gyerekét is, hogy ne csússzunk el az indexeléssel
    * Ebből következik, hogy nem fog sose kiürülni a sor, tehát nem a sor ürességéig megyek, hanem amíg a tömböt be nem járom - és nem is kell ellenőriznem a sort, mert tényleg sose lesz üres
    * Ha NULL-t olvasok,akkor a tömbben üresnek kell lennie a neki megfelelő indexen
    * Ha nem, akkor a kulcsnak kell lennie, ugyanitt ellenőrzöm az adott Node szülő-gyerek viszonyait is, nullcheckkel
    * A végén tehát n lesz i értéke, hiszen mindig a ciklusmag végén léptetjük i-t, az utolsó ciklusmag-lefutás után már kiindexelünk
    * Ekkor még marad a sorban NULL, de másnak nem szabadna lennie, ezt ellenőrizzük

    ```
    isTheSame(a : N[n], t : Node3*) : L
        HA t = NULL
            FOR i := 0 to n-1
                HA a[i] != ÜRES
                    return false
            return true
        KÜLÖNBEN
            q : Queue
            q.add(t)
            i := 0
            AMÍG i != n
                p := q.rem()
                HA p = NULL
                    HA a[i] != ÜRES
                        return false
                    q.add(NULL)
                    q.add(NULL)
                KÜLÖNBEN
                    HA a[i] != p->key() //ezek tudatosan nem VHA-k, külön IF-ek!
                        return false
                    HA p->left() != NULL és p->left()->parent() != p
                        return false
                    HA p->right() != NULL és p->right()->parent() != p
                        return false
                    q.add(p->left())
                    q.add(p->right())
                i := i+1
        AMÍG !q.isEmpty()
            HA q.rem() != NULL
                return false
        return true
    ```

    * Egyébként a t = NULL-os elágazás ki is vehető simán
    * Látható, a teljes, ideális megoldás elég hosszú (bár sok benne az ismétlődés, nem annyira bonyolult, mint elsőre tűnhet), így igyekeztem viszonylag magas pontszámot adni már a részsikerekre is
    * Sokak olyasmi megoldást próbáltak, amit az alábbiakban mutatok: egy tömbbe kiírták a tömbös pl. preorder bejárását, utána összehasonlították a láncoltéval
    * Ez nyilván kicsit kevésbé hatékony megoldás, de mint ötlet teljesen jó
        * Első körben végigmegyünk a tömbös ábrázoláson, és a preorder bejárás tömbjét kinullázzuk, mellesleg elöntjük, üres-e fa (ugyanis a preorder bejárásban volt egy olyan elágazás, hogy üres esetben ne csináljon semmit)
        * Ha üres a fa, megnézzük, láncoltan is üres-e, és kész is vagyunk
        * Ha nem az, elkészítjük a preorder bejárást, majd ellenőrizzük hogy azonos-e a két ábrázolás szerinti preorder bejárás
        * A végén még megnézzük a tömb maradék elemei üresek-e. Ez azért fontos, mert a tömb kapacitása nagyobb-egyenlő, mint a tényleges faméret és többek között ezzel tudjuk megállapítani, hogy valóban csak annyi elem volt lefoglalva, mint a másik ábrázolás szerint
        * Az is elég lenne, ha csak az első elemet néznénk meg a bejárás után, mert magában a preorder bejárásban már nincsenek lyukak
    * Egyébként ez a megoldás nehézkes is és nem is elég, mert attól, hogy két fa preorder bejárása ugyanaz, még nem lesz ugyanaz a két fa... ehhez tehát még kéne ugyanígy az inorder is, de "ennek meggondolását az olvasóra bízzuk"
        
    ```
    isTheSame(a : N[n], t : Node3*) : L
        pre : N[n]
        empty := true
        FOR i := 0 to n-1
            pre[i] := ÜRES
            HA a[i] != ÜRES
                empty := false
        HA empty
            return t = NULL
        KÜLÖNBEN
            indexPre := 0
            preorder(a, 0, pre, indexPre)
            indexPre := 0
            HA preorderCheck(t, pre, indexPre)
                FOR i := indexPre to n-1
                    HA pre[i] != ÜRES
                        return false
            KÜLÖNBEN
                return false
    ```
    
    * Az indexPre referencia szerint átadott változó mondja meg, hányadik elemet járjuk be épp, hiszen ez nem ugyanaz, mint a tényleges tömbbeli index, sőt, nem is ugyanaz a maximuma, a tömb kapacitása nagyobb lehet, mint a hasznos elemeinek száma
    * Első híváskor az a[0] biztos nem lesz üres, feltételezve, amúgy egy rendes fáról beszélünk, mert abban az ágban vagyunk, ahol nem volt az a üres
    * Utána már lehet akár üres is, ezeket mindig ellenőrizzük is a rekurzív hívások előtt (ez most eltérés az alap preorder alakhoz képest, de jelentősége nincs)

    ```
    preorder(a : N[n], index : N, pre : N[n], &indexPre : N)
        pre[indexPre] := a[index]
        indexPre := indexPre + 1
        HA 2*index+1 < n és a[2 * index + 1] != ÜRES
            preorder(a, 2 * index + 1, pre, indexPre)
        HA 2*index+2 < n és a[2 * index + 2] != ÜRES
            preorder(a, 2 * index + 2, pre, indexPre)
    ```
    
    * A preorderCheckben ugyanúgy preorder szerint megyünk végig, de most a láncolt verzión, a tömbösön pedig csak egyesével
    * Közben ellenőrizzük a szülő-gyerek kapcsolatokat is
    
    ```
    preorderCheck(t : Node3*, pre : N[n], &indexPre : N) : L
        HA t->key() != pre[indexPre]
            return false
        KÜLÖNBEN
            indexPre := indexPre + 1
            HA t->left() != NULL
                HA t->left()->parent != t
                    return false
                KÜLÖNBEN
                    return preorderCheck(t->left(), indexPre)
            HA t->right() != NULL
                HA t->right()->parent != t
                    return false
                KÜLÖNBEN
                    return preorderCheck(t->right(), indexPre)
    ```

* Írjunk algoritmust, aminek az inputja egy betűket tartalmazó n elemű tömb és egy [0..n-1]-es index. A tömb legyen egy bináris fa tömbös reprezentációja, az index pedig egy konkrét eleme ennek a fának. Az algoritmus adjon vissza egy számot, ami a megadott elem preorder bejárás szerinti rákövetkezője. Ha nincs ilyen, akkor az n számot adja vissza. Az algoritmus legyen minél hatékonyabb
    * A preorder bejárás sorrendje az "én-bal-jobb", azaz ha nem üres a bal részfa, akkor onnan következik, mégpedig a gyökér, hiszen arra is igaz ez a rekurzív szabály. Azaz, ha van bal gyerek, akkor a bal gyerek az. Ha nincs, akkor a jobb. Ha az sincs, akkor azt kell megnézni, a szülőnek melyik gyereke voltam. Maga a szülő már volt előttem, ha a jobb gyereke vagyok, akkor már a bal gyereke is, de ha a bal gyereke vagyok, akkor még a jobb gyereke hátra van. És ez rekurzívan is igaz, tehát ha valahol a gyökér felé vezető úton én a bal részfában vagyok, akkor annak az elemnek a jobb részfája (annak is a gyökere) a következő

    ```
    next(a : Ch[n], i : [0..n-1]) : [1..n]
        HA 2 * i + 1 < n és a[2 * i + 1] != ÜRES
            return 2 * i + 1
        HA 2 * i + 2 < n és a[2 * i + 2] != ÜRES
            return 2 * i + 2
        AMÍG alsóegészrész((i-1)/2) > 0 és 2 * (alsóegészrész((i-1)/2)) + 2 = i
            i := alsóegészrész((i-1)/2)
        HA i < 0
            return n
        KÜLÖNBEN
            return 2 * i + 2
    ```

    * Az első két HA magáért beszél - mivel itt returnölések vannak, nem kell VHA
    * Az utolsóban megnézem a szülő egyrészt létezik-e (nem indexelek-e ki, ha ez nem történik meg, akkor tuti nem lehet ÜRES), másrészt a szülő jobb gyereke-e az i
        * Ha ez van, akkor megyek a szülőre
    * Amint bal gyerek volt, annak a jobb gyereke a next
    * Ha pedig negatívba mentem, akkor return n
* Írjunk algoritmust, aminek az inputja egy absztrakt formában megadott bináris rendezőfa egy csúcsa (szülő referenciák vannak), az outputja pedig egy láncolt lista, amiben az összes, ennél a csúcsnál nagyobb kulcsérték szerepel szigorúan növekvő sorban. A maximális pontért az algoritmus legyen minél hatékonyabb. Az órán tanult algoritmusokat megfelelően paraméterezve meg szabad hívni
    * 3 megoldás is adódik:
        * Megkeressük az inorder szerinti rákövetkezőjét, majd annak is a rákövetkezőjét, stb.
        * Elkezdjük bejárni inorder módon, amikor a megadott elemet elérjük, onnantól elkezdjük beírni a listába - bár ezt csak az érdekesség kedvéért írom, mivel nem adtuk át a fa gyökerét, ez a feladatkiírás alapján nem járható út
        * Kigyűjtjük az összes elemet ami az inorder útvonalon rajta van és rendezve beszúrogatjuk a listába
    * Első megoldás:
        * Itt a next() az órai legutolsó, "rákövetkezés keresőfában" algoritmus
        * Készítek tehát egy listát, de ennek az utolsó elemét is számon tartom, hogy konstans módon szúrhassam a végére az egyre nagyobb elemeket (szig. mon. növő lista kell)
        * A feladatleírás alapján abban bízhatok, hogy p nem NULL, de persze a nextje már lehet az
        * De amíg nem az, mindig beszúrok. Ha az elejére, akkor oda, nem elfelejtve lastot is állítani, ha pedig a végére akkor oda
        * Mivel szig. mon. növő lista kell, és rendezőfával van dolgunk, ahol lehet ismétlődés, ezért ezeket ki is szűrjük
        ```
        biggerElements(p : BinTree) : E1*
            l := NULL
            last := NULL
            n := next(p)
            AMÍG n != ÜRES
                HA l = NULL
                    l := new E1
                    l->key := n.key()
                    last := l
                KÜLÖNBEN
                    HA last->key() != n.key()
                        q := new E1
                        q->key := n.key()
                        last->next := q
                        last := q
                n := next(n)
            return l
        ```
    * Második megoldás:
        * Mivel a gyökér nincs meg, ez nem lehetséges a feltételeinkkel, ezért inkább csak érdekesség
        * Természetesen hazudtam, minden lehetséges, a gyökeret meg lehet keresni, ezt fogjuk tenni
        ```
        biggerElements(p : BinTree) : E1*
            root := p
            AMÍG(root.parent() != ÜRES)
                root := root.parent()
            l := NULL
            last := NULL
            inorder(l, last, root, p.key())
            return l
        ```

        ```
        inorder(&l : E1*, &last : E1*, t : BinTree, k : T)
            HA t != ÜRES
                inorder(l, last, t.left(), k)
                HA t.key() > k
                    HA l = NULL
                        l := new E1
                        l->key := t.key()
                        last := l
                    KÜLÖNBEN
                        HA last->key() != t.key()
                            q := new E1
                            q->key := t.key()
                            last->next := q
                            last := q
                inorder(l, last, t.right(), k)
        ```
        * Ez a hagyományos inorder bejárás, a középen levő jó sok sor a "process()" megfelelője
        * Itt csak akkor érdekel minket a dolog, ha k-nál nagyobb a kulcs
        * De akkor megint eljátszuk az üres lista felülírását vagy a végére szúrást konstans időben
        * Vegyük észre, hogy mind az l, mind a last referencia szerint adódott át. Előbbi azért, mert az maga a kiszámolandó eredmény és mint ilyen, amikor kezdeti értéket kap, fontos, hogy ez a hívó közeg számára is látható legyen. Utóbbi pedig azért, mert nem "egyesével" nő, tehát amikor a preorder bejárás bal részfája mondjuk beszúr 5 elemet, akkor amikor a process()-hez érünk, valóban az 5. elemen kell állnia a lastnak, ezt csak így érhetjük el
    * Harmadik megoldás:
        * Elkészítjük megint a listát, és ugyanúgy a lista vége pointerre is szükségünk lesz a hatékony sorrendtartó beszúrás miatt
        * Megnézzük, van-e jobb részfa. Ha van, akkor az végig az inorder szerint a p után van, tehát bejárjuk
        * Most megnézzük a szülőt, illetve az őst, egész addig megyünk, amíg a p nem lesz a bal leszármazott. Ekkor a következő maga az az ős, majd utána az ő jobb részfája jön
        * Ez után megyünk tovább ugyanezzel a logikával, míg el nem érjük a gyökér nemlétező ősét
    
    ```
    biggerElements(p : BinTree) : E1*
        l := NULL
        last := NULL
        HA p.right() != ÜRES
            inorder(p.right(), l, last)
        parent := p.parent()
        AMÍG parent != NULL
            AMÍG parent != NULL és parent.right() = p
                p := parent()
                parent := p.parent()
            HA parent != NULL
                add(parent, l, last)
                HA parent.right() != ÜRES
                    inorder(parent.right(), l, last)
                p := parent
                parent := p.parent()
        return l
    ```
    * Mint látjuk, az inorder() és add() függvényeket kell megírni, mindkettő olyan, hogy az input üresség ellenőrzése már megvan
    ```
    inorder(t : BinTree, &l : E1*, &last : E1*)
        HA t.left() != ÜRES
            inorder(t.left(), l, last)
        add(t, l, last)
        HA t.right() != ÜRES
            inorder(t.right(), l, last)
    ```
    * Gyakorlatilag csak inorder szerint meghívogatjuk az add()-ot. Persze megint referencia az l és a last is, de ezt már tudjuk miért van így
    ```
    add(t : BinTree, &l : E1*, &last : E1*)
        HA l = NULL
            l := new E1
            l->key := t.key()
            last := l
        KÜLÖNBEN
            HA last->key() != t.key()
                q := new E1
                q->key := t.key()
                last->next := q
                last := q
        
    ```
    * Igazából ugyanazt csináltam végig, csak más struktúrában - remélem ez az összefoglaló segített megérteni a szükséges gondolkodásmódot akár a pótzh-ra, akár a vizsgára, akár csak úgy az eljövendőre