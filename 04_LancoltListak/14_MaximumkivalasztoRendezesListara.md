# Összehasonlító rendezések - Maximumkiválasztó rendezés

## Láncolt változat - Fejelemes listára

* Tehát a listatípus HL, az elemtípus E1, hiszen nem ciklikus és nem kétirányú, de f-fel jelölöm, mert fejelemes
* Ez kényelmes az üres lista kezelése miatt, de a feladat megoldható lenne persze SL-re is

```
maximumSelectionSort(f : E1*)
	rl: = NULL
	AMÍG f->next != NULL
		maxPrev := f
		max := f->next
		prev := f->next
		p := prev->next
		AMÍG p != NULL
			HA max->key < p->key
				maxPrev := prev
				max := p
			prev := p
			p := p->next
		maxPrev->next := max->next
		max->next := rl
		rl := max
	f->next := rl
```

* Az rl lesz egy rendezett (amúgy nem fejelemes) lista, amibe egyre csak gyűjteni fogjuk az elemeket, hasonlóan, mint a lista megfordítása és a beszúró rendezés feladatoknál
* A végén f fejelem mögé beállítjuk ezt a listát - ezzel lesz fejelemes, és ezért van, hogy f-et nem kell (azaz nem is szabad) referencia szerint átadni, mert maga f értéke nem változik
* Itt tehát nem egy tömb hátsó indexeire, hanem egy új listába gyűjtjük a már jó helyen levő elemeket
	* Természetesen az újakat a lista elejére rakjuk, mivel így hatékonyabb és mivel tudjuk, hogy a korábban kiválasztottak nagyobbak voltak, azaz eléjük szúrhatunk biztonsággal (minden más hiba lenne)
	* Ha nem így tennék, az kb. a beszúró rendezés lenne, kicsit rosszabb műveletigénnyel
* A külső ciklusban ahogy ezt a programozási tételből megismerhettük, elmentjük a maximumelemet (ami a lista első érdemi eleme), majd az ő rákövetkezőjét (ami már lehet null!) kinevezzük első vizsgálandó elemnek
* Végigmegyünk a (prev,p) kettőssel az elemeken, és ha a maxnál nagyobbat találok, frissítjük azt
* A végén a maxot kiláncolom (ezért kellett az előző), majd beláncolom a készülő listába
* A maxPrev a kiláncolás miatt kell, a prev pedig, hogy értéke tudjunk ennek a maxPrevnek adni
* Invariáns: prev->next = p, maxPrev->next = max, amíg bejártuk, maxban van a legnagyobb kulcs, és akik már rl-ben vannak, ők szigorúan rendezettek és az eredeti input legnagyobb elemei
* A műveletigény természetesen a dupla ciklus miatt négyzetes
    * Effektíve itt a külső ciklus nem n-1, hanem n körös, mert minden elemet át kell láncolni

## Láncolt változat - H2CL-ra

* Itt újabb tabukat döntögetünk, mivel a fejelemesség ellenére referencia szerint adom át a fejelemet
* Ez az algoritmus most úgy fog működni, hogy fejelemestül készíti el a rendezett, új listát, és a végén a fejelemet konkrétan átmutattatja erre - ez a döntés a példa miatt született, egyébként sok értelme nincs és a H2CL-séghez sincs köze

```
maximumSelectionSort(&f : E2C*)
	rf := new E2C
	AMÍG f->next != f
		max := f->next
		p := max->next
		AMÍG p != f
			HA p->key > max->key
				max := p
			p := p->next
		max->next->prev := max->prev
		max->prev->next := max->next
		HA rf->next == rf
			max->next := rf
			rf->prev := max
		KÜLÖNBEN
			max->next := rf->next
			rf->next->prev := max
		rf->next := max
		max->prev := rf
	q := f
	f := rf
	delete q
```

* Az elején tehát létrehozunk egy új node-ot, amit fejelemnek szánunk, ezért a kulcsa definiálatlan, de a két pointere saját magára kell mutasson
	* Az E2C default konstruktora pont ezt teszi, ezért explicit ezeket nem kell kiadnunk
* A külső ciklus feltétele a fejelemes lista "szemantikus" ürességét vizsgálja
* Most nem szórakozunk előző pointerekkel, mert vannak visszapointereink - nem kell előrenézés
* Megintcsak a fejelem következője a kezdeti max, és az azt követő - ami lehet hogy nem létezik, ami itt azzal ekvivalens, hogy visszamutat a fejelemre - lesz az első amit utána bejárunk
* Amikor megvan a max, az előzőjének a következőjét és a következőjének az előzőjét is frissítjük (azaz kiláncoljuk a listából)
* Ezután beláncoljuk a listába, ami első megközelítésben egy ilyen elágazás volt, ami később alakult, alább látható az egyszerűsített változat
    * Merthogy a különben ág általánosabb a felső ágnál (mivel spec. rf->next == rf)
* Végül beállítjuk a fejelem pointerét és töröljük a régit. Ez is egyszerűsödött kicsit:
    * Nem kell kimenteni segédváltozóba a fejelemet, mert rf-ben már megvan, minden ami kell
    * Illetve már "törölt" pointernek adhatunk új értéket, hiszen ilyenkor nem a pointert töröljük, hanem a heapbeli objektumot, amire mutatott
* Íme a javított változat:

```
maximumSelectionSort(&f : E2C*)
	rf := new E2C
	AMÍG f->next != f
		max := f->next
		p := max->next
		AMÍG p != f
			HA p->key > max->key
				max := p
			p := p->next
		max->next->prev := max->prev
		max->prev->next := max->next
		max->next := rf->next
		rf->next->prev := max
		rf->next := max
		max->prev := rf
	delete f
	f := rf
```
