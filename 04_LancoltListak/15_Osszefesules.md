# Rendezett HL-k rendezettséget megtartó összefésülése (uniója)

* Az alapfeladatnak többféle változata ismeretes annak függvényében, hogy...
    * Unióról, metszetről, különbségről (egyik vagy másik irányban), szimmetrikus differenciáról beszélünk
    * Mi legyen az inputok "sorsa" a függvény futásának végére (legyen benne valami más; ürüljön ki; legyen immutable, azaz változatlan, ...)
    * A műveletigényről is szoktak lenni elvárásaink
    * A halmaztulajdonság (minden elemből maximum egy van) is gyengíthető zsáktulajdonságra (multiset)
    * A rendezettség viszont mindig alapelvárás az összefuttatós algoritmusvázhoz (röviden halmaznál: szigorúan monoton legyen, zsáknál: monoton legyen - a növekvő vagy csökkenő tulajdonság is megengedett, de minden input és output e szempontból azonos)
* Itt most legyen az a feladat, hogy f1 és f2 unióját állítsuk elő f1-be, f2 pedig ürüljön ki
* Az OOP tárgyból ismert háromágú elágazás lesz a megoldás lelke: vesszük a két lista "következő" elemeit, és megnézzük a rendezettség szerint melyik a következő a közös felsorolásban. Azzal kezdünk valamit, majd igény szerint egyikben, másikban, mindkettőben továbblépünk
* A műveletigény itt Ordó(|f1|+|f2|) lesz. A ciklusban végigmegyek addig, amíg még egyik sem fogyott el. Ez akkor lehet maximális, ha f2-ben csupa kisebb elem van, mint f1-ben, de az utolsó eleme nagyobb. Ekkor egyesével végigmegyünk f2 elemein az utolsó kivételével, emiatt nem ér véget f2, tehát futni fog még a ciklus. Most végigmegyek f1 elemein, és ekkor léptem összesen f2 méretényi - 1 + f1 méretényit, ami már nagyságrendben így is annyi, mint amit állítottunk, de a végén még jön q átfűzése, ami 1 lépésnek számít, tehát ez kereken a két lista hosszányi lépésszámot fog tekerni. Ennél lehet jobb futási eredmény, ha pl. vannak közös elemek, vagy f2 összes eleme kisebb, mint f1 összes eleme, ezért "csak" Ordó és nem Théta
* Azt azért láthatjuk, hogy minden esetben lineáris a műveletigény, csak rossz esetben a két lista hosszának összege szerinti lineáris, jó esetben a két lista hossza közül vett kisebb hossza szerinti lineáris, ami - mivel nem tudjuk a két lista hosszának egymáshoz képest vett viszonyát - lehet nagyságrendekkel jobb is
* Bár már többször tisztáztuk, de nem lehet elégszer elmondani, mert gyakori hiba: ha az egyik, vagy mindkét lista hossza 0, az nem számít helyes érvnek amellett, hogy ekkor a műveletigénynek is 0-nak, vagyis legalábbis konstansnak kellene lennie. A műveletigényeket mindig az input méretének függvényében mondjuk, azaz ha n=0 és ekkor a műveletigény kb. 0, az semmit nem jelent, hiszen ekkor lineáris is épp úgy lehet, mint köbös. Amikor a "legjobb" inputot keressük, akkor egy olyan struktúrát keresünk, ahol adott méret mellett, ahhoz képest a lehető legkevesebb műveletet kell végeznünk. Pl. egy rendezőalgoritmus esetében, ha az input eleve rendezett, akkor 0 cserét kell végezünk, míg ha nem az, akkor biztosan többet
* A naiv megközelítés az lehetne, ha külső ciklusban veszem rendre f1 elemeit, s minden körben az aktuális f1-belihez veszem f2 minden elemét, |f1|*|f2| (négyzetes) műveletigényt produkálva

```
union(f1 : E1*, f2 : E1*)
	pPrev := f1
	p := f1->next
	q := f2->next
	AMÍG p != NULL és q != NULL
		VHA p->key < q->key
			pPrev := p
			p := p->next
		VHA p->key > q->key
			pPrev->next := q
			q := q->next
			pPrev := pPrev->next
			pPrev->next := p  
		VHA p->key = q->key
			pPrev := p
			p := p->next
			s := q
			q := q->next
			delete s
	HA q != NULL
		pPrev -> next := q
	f2->next := NULL
```

* Elindulunk egy-egy pointerrel a két listán
	* Kezdhetünk az fx->nextekkel, mert az első elem a fejelem, ami nem játszik
	* f1-en számontartjuk az előző pointert is (pPrev), mert ide fogunk beszúrni f2-ből ha úgy van (és általában úgy van)
* Jön a jól ismert három ágú elágazás
	* Ha az f1-beli a kisebb, csak továbblépünk
	* Ha az f2-beli az, akkor p előzője és p közé szúrjuk (hiszen p előzője már kisebb-egyenlő volt, mert az már az unió része). Tehát pPrev következője legyen ez az elem, léptessük tovább az f2 pointerét, q-t, hogy ne ragadjon be, pPrevet is állítsuk az előbbi q-ra (amit már továbbléptettünk persze), hiszen most már ez az elem a p előzője, majd végül ennek az elemnek a következőjét is állítsuk p-re
	* Ha egyenlő a két elem, akkor egyrészt azt csináljuk (első két sor), mint az első ágon - megtartjuk az f1-belit -, másreszt kidobjuk az f2-belit, de előbb továbbléptetjük a q pointert
* A végén egy dolog nem fordulhat elő: mindkét listában van még feldolgozatlan elem (ciklusfeltétel miatt)
	* Az lehet, hogy egyikben sincs, ekkor kész vagyunk
	* Az lehet, hogy f1-ben van, ekkor is, hiszen ők kizárásos alapon nagyobbak f2 elemeinél
	* És az is lehet, hogy f2 nem üres, ekkor szimplán csak pPrev utánra rakjuk f2 maradékrészét, ami q-val keződik (azért pPrev utánra, mert p már NULL, onnan tudjuk, hogy f1-en végigmentünk - ha a NULL p-t írnánk át, az nem szúrna a listába!)
* A q != NULL feltétellel szemantikailag ekvivalens a p == NULL feltétel, viszont ott ha q is NULL, akkor ez egy felesleges nullpointer-átláncolást jelent, azaz ez egy picit hatékonyabb
    * És mi van, ha nincs feltétel egyáltalán? Az hülyeség volna, mert ha q == NULL, de pPrev->next nem az, ami lehetséges, ha p nem NULL, akkor felülírjuk azt helytelenül NULL-lal
* A legeslegvégén még NULL-ra álítjuk f2 nextjét, hiszen ez továbbra is a hajdanvolt első elemére mutat, ami most "valahol" f1-ben van már, tehát a nextje egyáltalán nem biztos, hogy f2-beli (na meg ha a fejelem nextje null, az jelenti az üres listát és épp ez volt a feladat)
* Ez az "éselős" ciklusfeltételes megoldás, de létezik "vagyolós" is. Erre is fogunk majd látni példát

## A "VHA" jelölésről

* A félév során normálisan struktogramokat rajzolnánk, ahol szándék szerint a fenti 3 ágú elágazás ilyen vizuális sémát követne:

```
 __________________________________________
|\            |\            |\            |
| \ feltétel1 | \ feltétel2 | \ feltétel3 |
|__\__________|__\__________|__\__________|
|             |             |             |
|      S1     |      S2     |      S3     |
|_____________|_____________|_____________|
```

* Ez igen szemléletes jelölés, ez így egy "darab" elágazást jelent, ami speciel 3 ágú
    * Amikor a vezérlés ide ér, véletlenszerűen (!) teszteli az egyik ágat (tehát nem feltétlen feltétel1 - feltétel2 - feltétel3 sorrendben), ha igaz, lefuttatja a hozzá tartozó kódot, és megy tovább az elágazás utánra; ha nem, megvizsgál még egy ágat, stb.
    * Biztosan nem fog több ág kódja lefutni, akkor se, ha valamelyik kettő (vagy akár több) ág feltétele átfed
    * Ha nem minden eset van lefedve, az nem gond, de ha a vezérlés nem tud ráfutni egy ágra se, az hibaállapotot (ABORT) jelent
        * Pl. ha feltétel1 azt jelenti, hogy x=1, feltétel2, hogy x=2 és feltétel3, hogy x páros, akkor x=2 esetben vagy S2 vagy S3 fut le 50-50% eséllyel, de x=3 esetben a program elszáll. Ha biztosak vagyunk benne, hogy x ezen a ponton csak páros lehet vagy 1, akkor a program nem hibás
* A Java és C++ programozási nyelvekben ilyen alakzat nincs
    * Ezt if-else if-else if konstrukcióval tudjuk szimulálni, ami viszont sorrendiséget és egymást kizárást "ad" nekünk
        * Tehát az előző példánál maradva, ha előbb biztosan x=1-re tesztel, ha ez hamis, akkor nézi meg x=2-t, és ha ez hamis, akkor hogy x páros-e. Itt egyértelmű, hogy átfedő esetekben az előbb leírt ág élvez elsőbbséget
        * Ha egyik ág se teljesül, a C-típusú nyelvek ifjei (akárhány ágúak is legyenek) implicit generálnak egy else ágat SKIP programmal, tehát nem fog ekkor se elszállni a futás
* Ezt így lehetne struktogrammal megfogni:

```
 _________________________
|\                       /|
| \      feltétel1      / |
|__\___________________/__|
|    |\                  /|
| S1 | \   feltétel2    / |
|    |__\______________/__|
|    |    |\             /|
|    | S2 | \ feltétel3 / |
|    |    |__\_________/__|
|    |    |    |          |
|    |    | S3 |   SKIP   |
|____|____|____|__________|
```

* Ahol a jobb alsó SKIP az "implicit" else ág megfelelője
    * Ebből is látható, hogy a struktogramban nincs implicit else ág - hisz itt is explicit ki van írva, nincs olyan, hogy egy kétágú elágazás alatt ne két rubrika legyen
    * A több ágú elágazás egy ága alatt viszont valóban egy kód van, és nem is lehet olyat mondani n+1. ágnak, hogy "else", hanem, ha ezt szeretnénk, az összes ág feltételét össze kéne vagyolni és ezt tagadni
* A fentiekből látszik, hogy voltaképen ez nem is egy többágú elágazás, hanem két ágú elágazások egymásba ágyazása
* A kettő nagyon hasonló, és az esetek nagy százalékában csereszabatos, de mégse ugyanaz
* A mi konkrét esetünkben, a struktogramnyelv 3 ágú elágazással tudjuk a legkönnyebben és a legszemléletesebben kifejezni, amit szerettünk volna
* Ezt próbáltam pszeudokódra fordítani - első körben egy szinten levő HA - HA - HA alakkal, amit viszont ha tovább fordítunk C++ kódra, akkor egy ilyen strukrogramnak megfelelő kódot kapunk:

```
 _______________
|\             /|
| \ feltétel1 / |
|__\_________/__|
|    |          |
| S1 |   SKIP   |
|____|__________|
|\             /|
| \ feltétel2 / |
|__\_________/__|
|    |          |
| S2 |   SKIP   |
|____|__________|
|\             /|
| \ feltétel3 / |
|__\_________/__|
|    |          |
| S3 |   SKIP   |
|____|__________|
```
* Esetünkben ez nem jó megoldás, mert ha az első ág továbblépteti p-t, a második ág p->key-e már az eredeti p nextjét kérdezi le és ezt a lekérdezést nulcheckkel kellett volna védenünk
* Az biztos, hogy nem akarhatunk "KÜLÖNBEN HA"-kat írni a HA - HA - HA helyére, mert absztrakt szinten nem ez a kód (felesleges bonyolítás lenne), hiába oldanánk meg C++-ben úgy
* Ezért vezettem be a "VHA" kifejezést, ami jelentse azt, hogy az egy szinten és egymás után levő VHA-k egymás alternatívái, egy darab többágú elágazást valósítanak meg, legfeljebb egyikük fut le, de a kiértékelés sorrendje nemdeterminisztikus
    * Struktogramos megfelelője az itteni első
    * Kódolásos megfelelője nincs, de jó közelítéssel szimulálható az itteni második struktogramnak megfelelő kóddal, és bizonyos esetekben akár a 3-nak megfelelő kóddal is
        * Ugyanakkor ezt mindig az adott kód vizsgálata utána döntsük el
