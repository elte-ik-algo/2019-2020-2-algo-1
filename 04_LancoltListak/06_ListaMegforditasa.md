# S1L megfordítása

* Az algoritmus alapelgondolása a következő:
	* Mentsük ki egy pointerrel a lista első elemét, majd állítsuk NULL-ra a listát
	* A kimentett p pointer segítségével menjünk végig a listán (lépkedünk tehát, amíg p nem NULL)
		* Az aktuális elemet mentsük ki (csak hogy p-vel tovább tudjunk lépni), majd fűzzük be az újraépülő listánk elejére
			* Ez két lépés: a kimentett elem nextjét l-re, majd l-t az elemre állítjuk

	```
	reverse(&l : E1*)
	  p := l
	  l := NULL
	  AMÍG p != NULL
	    q := p
	    p := p->next
	    q->next := l
	    l := q
	```

* Az első két sor az új lista inicializálása és a régi "kimentése"
* A ciklusmag első két sora a továbblépés, de úgy, hogy az aktuális elemet mégis elérjük még
* A második két sor pedig a lista elejére való visszaláncolás
* Vegyük észre, hogy üres és egy elemű listára is működik, bár kicsit felesleges a működése (mindig ellenőrizzük a memóriaszivárgást)
* Az algoritmus "helyben" működik, azaz egy "void", nem tér vissza semmivel, hanem a paraméterrel kezd valamit. Emiatt is adtuk át referencia szerint l-t
* Komplexitása lineáris, hiszen végig megyünk egyszer a listán, de azon belül már csak a konstans műveletigényű elejére szúrást alkalmazzuk