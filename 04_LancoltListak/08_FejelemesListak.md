# Fejelemes listák (HL)

* Az ilyen listák pointerét hagyományosan f-fel jelöljük
* Ez a bizonyos kezdőpointer egy úgynevezezett fejelemre (strázsa, sentinel) fog mutatni
* Ez az elem fizikailag ugyanolyan E1 (eseleg E2 vagy E2C, lásd később), mint a többi, de szemantikailag nem jelöl listaelemet
	* Azaz egy n eleműnek kezelt lista fizikailag n+1 elemű lesz
	* f sose lehet nullpointer, ami könnyebbé teszi a műveleteinket
* Az esetek túlnyomó többségében f-nek nem definiált a kulcs része (bár a listára vonatkozó metaadatokat, pl. a méretét tárolhatjuk benne, ha T szám típus)
    * Ebben a konkrét esetben a listahossz lekérdezés konstanssá válik a lineáris helyett, de persze minden a hosszt módosító függvényben ezt a tárolt értéket frissíteni kell - konstans áron
* Grafikusan úgy szoktuk jelölni hogy a tetejére és aljára egy-egy vízszintes vonalat húzunk

## Rendezett beszúrás H1L-be
	
* Az algoritmus ismerős lehet egy másik algoritmus részeként, az S1L-ba való egy pointeres rendezett beszúrás "vége" nézett ki egy az egyben így
* Úgy általában az egy pointeres, "előrenéző" változatot szeretjük itt használni, hiszen az első elem egyrészt biztosan létezik, másrészt nem hordoz figyelembe veendő adatot
* f-et nem kell referencia szerint átadni, hiszen maga f listaelem nem jön létre, nem is törlődik (törlésnél se), maximum a nextje módosulhat

```
insert(f : E1*, q : E1*)
	p := f
	AMÍG p->next !=NULL és p->next->key < q->key
		p := p->next
	q->next := p->next
	p->next := q
```